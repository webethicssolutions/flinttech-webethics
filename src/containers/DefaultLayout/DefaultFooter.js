import React, { Component } from 'react';
import PropTypes from 'prop-types';


const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
	

		<footer className="footer text-white pt-4">
            <div className="container"> 
                <div className="row">
                    <div className="col pt-6">
							<h6 className="mb-1 mb-lg-4 text-uppercase">Information</h6>
							<p className="mb-2"><i className="fas fa-clock pr-2"></i>Hours: 9:00-17:00</p>
                            <p className="mb-2"><a href="tel:724-307-8310" className="text-white"><i className="fas fa-phone fa-flip-horizontal mr-2"></i>724-307-8310</a></p>
                           <p className="mb-2"> <a href="mailto:info@flinttech.com" className="text-white"><i className="fas fa-envelope pr-2"></i>info@flinttech.com</a></p>
							<h6 className="mb-1 mt-4">Follow us on</h6>
							<ul className="list-inline follow-us">
							<li className="list-inline-item"><a href="https://twitter.com/flinttech" target="_blank" rel="noopener" title="twitter"><i className="fab fa-twitter"></i></a></li>
							<li className="list-inline-item"><a href="https://www.facebook.com/Flinttech" target="_blank" rel="noopener" title="facebook"><i className="fab fa-facebook"></i></a></li>
							<li className="list-inline-item"><a href="https://www.instagram.com/flinttechsolutions/" target="_blank" rel="noopener" title="instagram"><i className="fab fa-instagram"></i></a></li>
							<li className="list-inline-item"><a href="https://www.linkedin.com/company/flint-technologies-solutions/" target="_blank" rel="noopener" title="linkedin"><i className="fab fa-linkedin"></i></a></li>
				
							</ul>
                    </div>

					<div className="col pt-6 address-col">
						<h6 className="mb-1 mb-lg-4 text-uppercase">Address</h6>
						<p className="mb-3 mt-3 d-flex"><i className="fas fa-map-marker-alt mr-2"></i>
						<a href="https://goo.gl/maps/9gLM6FJNsVr" target="_blank" className="text-white">Headquarters : 11676 Perry 
						Highway<br /> Wexford, PA 15090</a>
						</p>
						<p className="d-flex"><i className="fas fa-map-marker-alt mr-2"></i><a  target="_blank" href="https://goo.gl/maps/5wDTxdPR8kt" className="text-white">Location : 126 Sierra Street<br />  El 
						Segundo, CA 90245</a></p>								
					</div>							
                </div>
            </div>
			
			<div className="footer-bottom mt-4">
					<div className="container">
					  <div className="row align-items-center">
						<div className="col-md-6 text-center text-md-left">
						  <p className="mb-md-0">© 2019 Flint Tech.  All rights reserved.</p>
						</div>
						<div className="col-md-6">
						  <ul className="list-inline mb-0 mt-2 mt-md-0 text-center text-md-right">
							<li className="list-inline-item"><a href="#/">Privacy Policy / </a></li>
							<li className="list-inline-item"><a href="#/">Career</a></li>
						  </ul>
						</div>
					  </div>
					</div>
			</div>	
		 </footer>
		
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;

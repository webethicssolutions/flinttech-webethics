import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import {
  AppFooter,
  AppHeader,
   AppSidebarNav,
} from '@coreui/react';
// sidebar nav config

// routes config
import jQuery from 'jquery';

import routes from '../../routes';

import "../../scss/t-scroll/t-scroll.min.css";
import navigation from '../../_nav';

import "bootstrap/dist/js/bootstrap.min.js";

import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';


class DefaultLayout extends Component {
	
	constructor(props) {
		super(props);
		this.handleLoad = this.handleLoad.bind(this);
		
	}
	
	componentDidMount() {
			jQuery.easing.def = 'easeInBounce';
			jQuery( ".loader" ).animate({
				'border-radius': "50%",
				'top':  "-200%",
             },800);
	}
	handleLoad() {
		
	}
	
	
	
  render() {
	
	/* const { loading } = this.state;
	console.log(loading ); 
    if(loading) { // if your component doesn't have to wait for an async action, remove this block 
		//return null;//setTimeout(() => this.setState({ loading: false }), 1500); // render null when app is not ready
	} */
	
    return (
      <div className="app">
		  <DefaultHeader />
		  <main className="main">
          
              <Switch>
                {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )}
				
				<Redirect from="/" to="/" />
				
              </Switch>
           
          </main>
		  <DefaultFooter />
         
	 </div>
    );
  }
}

export default DefaultLayout;

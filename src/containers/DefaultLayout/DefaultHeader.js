import React, { Component } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';


import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";

import jQuery from 'jquery';
import "bootstrap/dist/js/bootstrap.min.js";
import ReactBootstrap from 'react-bootstrap';
import logo from '../../assets/images/logo.jpg'
import ModalComponent from './ModalComponent';

//import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';



//var Modal = require('react-bootstrap-modal');

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
	constructor(props, context) {
		super(props, context);
		
	}

  
	render() {
	  /* let closeModal = () => this.setState({ open: false })
	  const loadingsState = this.state; */
	 

	// eslint-disable-next-line
    const { children, ...attributes } = this.props;
	if(window.location.pathname === '/'){
		var classadd = 'navbar navbar-expand-lg fixed-top';
	}else{
		var classadd = 'navbar navbar-expand-lg';
	}
	if(window.location.pathname !== '/'){
		var classadd1 = 'collapse navbar-collapse inner-header';
	}else{
		var classadd1 = 'collapse navbar-collapse';
	}
    return (
      <React.Fragment>
		<nav className={classadd} >
			<div className="container">
			  <Link className="navbar-brand"  to="/"><img src={logo} alt="" /></Link>
			  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
				<i className="fas fa-bars"></i>
			  </button>
				
				
				
				  <div className={classadd1} id="navbar-menu">
					<ul className="navbar-nav ml-auto">
						<li className="nav-item">
							<Link className={`nav-link ${window.location.pathname === '/' ? 'active' : ''}`} to="/">Home</Link>
						</li>
						<li className="nav-item">
							<Link className={`nav-link ${window.location.pathname === '/about' ? 'active' : ''}`} to="about">About</Link>
						</li>
						<li className="nav-item dropdown">
							<Link className={`nav-link dropdown-toggle ${window.location.pathname === '/security-operation' || window.location.pathname === '/cio-and-ciso' || window.location.pathname === '/risk-management' || window.location.pathname === '/business-enablement' ? 'active' : ''}`} id="dropdown01" to="/security-operation"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</Link>
							<div className="dropdown-menu" aria-labelledby="dropdown01">
								<Link className={`dropdown-item ${window.location.pathname === '/security-operation' ? 'active' : ''}`} to="/security-operation">Security Operations</Link>
								<Link className={`dropdown-item ${window.location.pathname === '/risk-management' ? 'active' : ''}`} to="/risk-management">Risk Management</Link>
								
								{/* <Link className={`dropdown-item ${window.location.pathname === '/business-enablement' ? 'active' : ''}`} to="/business-enablement">Business Enablement</Link> */}
								
								<Link className={`dropdown-item ${window.location.pathname === '/cio-and-ciso' ? 'active' : ''}`} to="/cio-and-ciso">CIO & CISO Consulting</Link>
							</div>
						</li>
					 
					 
					 {/* <li className="nav-item">
							<Link className={`nav-link ${window.location.pathname === '/resources' ? 'active' : ''}`} to="resources">Resources</Link>
					</li> */}
					</ul>{/*  */}
					
					<ModalComponent />
					
				  </div>
			
			  </div>
			</nav>
			
			<div>
				
			  </div>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;

import React from 'react';
import Loadable from 'react-loadable'

import DefaultLayout from './containers/DefaultLayout';

function Loading() {
  return <div className="loader"></div>;
}


const Home = Loadable({
	loader: () => import('./views/Home/Home/'),
	loading: Loading,
});

const Services = Loadable({
	loader: () => import('./views/Services/Services/'),
	loading: Loading,
});
const ConsultingServices = Loadable({
	loader: () => import('./views/Services/ConsultingServices/'),
	loading: Loading,
});
const RiskManagement = Loadable({
	loader: () => import('./views/Services/RiskManagement/'),
	loading: Loading,
});
const BusinessEnablement = Loadable({
	loader: () => import('./views/Services/BusinessEnablement/'),
	loading: Loading,
});

const About = Loadable({
	loader: () => import('./views/About/About/'),
	loading: Loading,
});


const Pricing = Loadable({
	loader: () => import('./views/Pricing/Pricing/'),
	loading: Loading,
});

  
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
	{ path: '/', exact: true, name: 'Home', component: Home },
	{ path: '/security-operation', exact: true,  name: 'Services', component: Services },
	{ path: '/cio-and-ciso', exact: true,  name: 'Services', component: ConsultingServices },
	{ path: '/risk-management', exact: true,  name: 'Services', component: RiskManagement },
	{ path: '/business-enablement', exact: true,  name: 'Services', component: BusinessEnablement },
	{ path: '/about', exact: true,  name: 'About', component: About },
	{ path: '/resources', exact: true,  name: 'Pricing', component: Pricing },
	
  
];

export default routes;

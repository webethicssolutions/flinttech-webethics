import './polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter as Router, Route, Switch,HashRouter} from 'react-router-dom';

// disable ServiceWorker
// import registerServiceWorker from './registerServiceWorker';

//ReactDOM.render(<App />, document.getElementById('root'));

ReactDOM.render(
	
  <Router>
      <App />
  </Router>,
  document.getElementById('root')
);

// disable ServiceWorker
// registerServiceWorker();

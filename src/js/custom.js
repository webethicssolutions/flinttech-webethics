import React, { Component } from 'react';
import { $,jQuery } from 'jquery';
import slick from "slick-carousel";
var Tu = require('t-scroll/public/theme/js/t-scroll.min.js');

jQuery(document).ready(function(){


Tu.tScroll({
	't-element': '.ts'
});

Tu.tScroll({
	't-element': '.main-banner .lightSpeedLeft',
	't-duration': 0.07,
	't-delay': 1
});
Tu.tScroll({
	't-element': '.main-banner .fadeUp',
	't-delay': 2
});

// Pricing switcher button
jQuery(".switcher__button").on('click', function(e) { 
	$(".switcher__button").toggleClass('switcher__button--enabled');
	$(".pricing__value").removeClass('pricing__value--hidden');
	$(".pricing__value").toggleClass('pricing__value--show pricing__value--hide');	
});

//
load_animation();
function load_animation(){
           jQuery( ".loader" ).animate({
               'border-radius': "50%",
               'top':  "-200%",
             }, 400, function() {
               // Animation complete.
             });
       }
	
});





jQuery(document).scroll(function() {
    var scroll = jQuery(window).scrollTop();
    if (scroll >= 80) {
        jQuery("body").addClass("header-sticked");
    }else{
    	jQuery("body").removeClass("header-sticked");
    }
});
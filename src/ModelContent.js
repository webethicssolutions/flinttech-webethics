export function AllModelContent(){
	var ModelContent = [];
	ModelContent['buttonHeading'] = 'Let`s Start Together';
	ModelContent['formHeading'] = 'Free Project Quote';
	ModelContent['formDescription'] = 'Fill out the enquiry form and we`ll get back to you as soon as possible.';
	ModelContent['quoteHeading'] = 'Get a<br /> Free Quote';
	ModelContent['quoteDescriptions'] = '"At Flint Tech Solutions, we use technology as the integrator to solve business problems. Our dedicated team delivers value and efficiency while maintaining the highest level of customer service and support."';
	ModelContent['owner'] = 'Ken Barrett';
	ModelContent['headquarter'] = '<a href="https://goo.gl/maps/9gLM6FJNsVr" target="_blank">11676 Perry Highway<br /> Wexford, PA 15090</a>';
	ModelContent['location'] = '<a target="_blank" href="https://goo.gl/maps/5wDTxdPR8kt">126 Sierra Street <br />El Segundo, CA 90245</a>';
	ModelContent['email'] = 'info@flinttech.com';
	ModelContent['phone'] = '724-307-8310';
	ModelContent['emailTitle'] = 'Email Us: ';
	ModelContent['phoneTitle'] = 'Phone: ';
	ModelContent['phone'] = '724-307-8310';
	ModelContent['thanksTitle'] = 'Thank You!';
	ModelContent['thanksMessage'] = "We've got your response! We'll get back to you shortly";
	return ModelContent;
}

import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch,HashRouter} from 'react-router-dom';

import './App.css';
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css';
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application

import jQuery from 'jquery';
// Containers
import { DefaultLayout } from './containers';
import { About } from './views/About/';




import { renderRoutes } from 'react-router-config';

class App extends Component {
	
	
  render() {
	
	const forceRefresh ={
		 forceRefresh:true,
	}
	return (
		   
		<Router {...forceRefresh}>
			<Route path="/" name="Home" component={DefaultLayout} />
		 </Router>
		 
    );
  }
}

export default App;

import Login from './Login';
import Home from './Home';
import Page404 from './Page404';
import Page500 from './Page500';
import Register from './Register';

export {
  Login,  Home, Page404, Page500, Register
};
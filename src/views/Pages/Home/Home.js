import React, { Component } from 'react'; 
import { Alert, Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row} from 'reactstrap';

import { Redirect} from 'react-router-dom';
 

class Home extends Component { 
	constructor() {
		super();
	}

	render() { 
		return (
		  <div class="main-banner">
			<div class="container">
				<div class="row">
					<div class="col col-xl-6 text-center text-xl-left">
					<h1 class="display-3">
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="1">I</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="2">n</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="3">c</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="4">r</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="5">e</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="6">a</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="7">s</span>
						<span class="font-weight-light lightSpeedLeft  t-animated pr-3" data-t-show="8">e</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="9">Y</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="10">o</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="11">u</span>
						<span class="font-weight-light lightSpeedLeft  t-animated" data-t-show="12">r</span>
						<br />
						<span class="lightSpeedLeft   t-animated" data-t-show="13">B</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="14">u</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="15">s</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="16">i</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="17">n</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="18">e</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="19">s</span>
						<span class="lightSpeedLeft   t-animated pr-3" data-t-show="20">s</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="21">V</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="22">i</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="23">s</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="24">i</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="25">b</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="26">i</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="27">l</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="28">i</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="29">t</span>
						<span class="lightSpeedLeft   t-animated" data-t-show="30">y</span>					
					</h1>
					<p class="lead fadeUp t-animated">Secure Your Data | Optimize Operations | Generate New Revenue </p>
					<p class="fadeUp t-animated">Good marketing makes the company look smart. Great marketing makes the customer feel smart.</p>
					<div class="button-wrap mt-4 mt-sm-5">
						<a class="btn btn-primary fadeUp ts "> GET STARTED</a>
						<a class="btn btn-outline-primary fadeUp ts">Contact Us</a></div>				
					</div>
				</div>
			</div>	
		</div>
		);
    }
}

export default Home;

import React, { Component } from 'react'; 
import { Alert, Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row} from 'reactstrap';

import { Redirect} from 'react-router-dom';
 
function validate(email, upassword) {
	// we are going to store errors for all fields
	// in a signle array
	const errors = [];  
	if (email.length <= 0) {
		errors.push("Please enter email.");
		/* }else if (email.split('').filter(x => x === '@').length !== 1) { */
	}else if (email.length > 0) {
		/* errors.push("Email should contain a @ \n"); */ 
		let lastAtPos  = email.lastIndexOf('@');
		let lastDotPos = email.lastIndexOf('.');

		if (!(lastAtPos < lastDotPos && lastAtPos > 0 && email.indexOf('@@') == -1 && lastDotPos > 2 && (email.	
			length - lastDotPos) > 2)) { 
			errors.push("Email is not valid.");
		} 
	} 
   if (upassword.length <= 0) {  
     errors.push("Please enter password.");
   }

  return errors;
}

class Login extends Component { 
	constructor() {
		super();
		this.state = {
			email: '',
			password: '',

			everFocusedEmail: false,
			everFocusedPassword: false,
			inFocus: '',
		};
	}
  
    state = { showError: true }
  
	handleEmailChange = (evt) => {
		this.setState({ email: evt.target.value });
	}
  
	handlePasswordChange = (evt) => {
		this.setState({ password: evt.target.value });
	}
  
	handleSubmit = (evt) => { 
		/*  if (!this.canBeSubmitted()) {
			evt.preventDefault();
			return;
		} */
		const { email, password } = this.state;
		/* alert(`Signed up with email: ${email} password: ${password}`); */  
		/* this.setState((prevState, props) => { 
			return { showError: !prevState.showError } 
		 }) 
		*/
		   
		const errors = validate(email, password);
		if (errors.length > 0) { 
			this.setState({ showLoginError: "" });
			this.setState((prevState, props) => { 
				return { showError: errors } 
			 })  
		}else{ 
			var url = 'http://nodeapplogin.herokuapp.com/api/admin/login'; 
			/* fetch(url)
			  .then(function(response) {
				if (response.status >= 400) {
					alert("response "+response.status);
				  throw new Error("Bad response from server");
				}
				return response.json();
			  })
			  .then(function(data) {
				  alert(data.person);
				//that.setState({ person: data.person });
			  }); */
			  
			  
			fetch(url, {
			  method: 'POST',
			  headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			  },
			  body: JSON.stringify({
				email: email,
				password: password,
			  }),
			}).then((response) => response.json())
				.then((responseJson) => { 
					 if(responseJson.success == true){
						 if(responseJson.admin_token !=""){ 
							localStorage.setItem('admin_token', responseJson.admin_token);
						 }
						this.setState({ showSuccess: responseJson.message });
					}else{
						this.setState({ showLoginError: responseJson.message });
					}  
				})
				.catch((error) => {
				  console.error(error);
				});
		} 
    }
  
	/* canBeSubmitted() {
		const errors = validate(this.state.email, this.state.password);
		const isDisabled = Object.keys(errors).some(x => errors[x]);
		return !isDisabled;
	} */
	
	render() { 
	console.log(localStorage.getItem('admin_token'));
		{localStorage.getItem('admin_token') && <div className="error-message"><Alert color="danger">{localStorage.getItem('admin_token')}</Alert></div>} 
		const errors = validate(this.state.email, this.state.password);
		/* const isDisabled = Object.keys(errors).some(x => errors[x]); */
		return (
		  <div className="app flex-row align-items-center">
			<Container>
			  <Row className="justify-content-center">
				<Col md="8">
				  <CardGroup>
					<Card className="p-4">
					  <CardBody>   
						<h1>Login</h1> 
						<p className="text-muted">Sign In to admin panel</p>
						
					    {this.state.showLoginError && <div className="error-message"><Alert color="danger">{this.state.showLoginError}<br/></Alert></div>} 
						{this.state.showSuccess && <Redirect to="/dashboard" />}
						{ /*<Form action="" method="post" encType="multipart/form-data" className="form-horizontal" onSubmit={this.loginSubmit.bind(this)}> */} 
						<form className="form-horizontal" onSubmit={this.handleSubmit} >
							 {this.state.showError && <p>{errors && errors.map(error => (
								<Alert color="danger" key={error}>{error}</Alert>
								))}</p>}
						<InputGroup className="mb-3">
						  <InputGroupAddon addonType="prepend">
							<InputGroupText>
							  <i className="icon-user"></i>
							</InputGroupText>
						  </InputGroupAddon>
						  <Input type="text" placeholder="Username" className={errors.email ? "error" : ""} type="text" placeholder="Enter email" value={this.state.email} onChange={this.handleEmailChange} />
						</InputGroup>
						<InputGroup className="mb-4">
						  <InputGroupAddon addonType="prepend">
							<InputGroupText>
							  <i className="icon-lock"></i>
							</InputGroupText>
						  </InputGroupAddon>
						  <Input type="password" placeholder="Password" className={errors.password ? "error" : ""} type="password" placeholder="Enter password" value={this.state.password} onChange={this.handlePasswordChange} />
						</InputGroup>
						<Row>
						  <Col xs="6">
							<Button color="primary" className="px-4">Login</Button>
						  </Col>
						  <Col xs="6" className="text-right">
							<Button color="link" className="px-0">Forgot password?</Button>
						  </Col>
						</Row>
						</form>
					  </CardBody>
					</Card>
					<Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
					  <CardBody className="text-center">
						<div>
						  <h2>Admin Login</h2>
						  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
							labore et dolore magna aliqua.</p>
							{ /* <Button color="primary" className="mt-3" active href="#/register">Register Now!</Button> */}
						</div>
					  </CardBody>
					</Card>
				  </CardGroup>
				</Col>
			  </Row>
			</Container>
		  </div>
		);
    }
}

export default Login;

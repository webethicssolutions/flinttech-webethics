import React, { Component } from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import layout from '../Services/layout.svg'
import analysis from '../Services/analysis.svg'
import grid from '../Services/grid.svg'
import SVG from 'react-inlinesvg';
import * as util from './HomeContent.js';


class Services extends Component {

	
  render() {
	const Services = util.Services();
   										
    return (
		<div>
			<div className="container my-5">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1600" data-aos-delay="300" >{Services['title']}</h2>
							<div className="clearfix"></div>
							<div className="brd"></div>
						</div>
					</div>
				</div>	
			</div>	
			
			
<div className="container service-sec mb-5">
        <div className="row">
				
				  <div className="col-md-4 col-lg-3 text-center">
					
					<SVG src={analysis} />
					<h3 className="my-4 text-uppercase">{ReactHtmlParser(Services['services_2_title'])}</h3>
					<p>{Services['services_2_description']}</p>
					<a className="btn btn-primary fadeUp my-3" href="/security-operation">Read More</a>
				  </div>
				  <div className="col-md-4 col-lg-3 text-center">
					<SVG src={layout} />
					<h3 className="my-4 text-uppercase">{ReactHtmlParser(Services['services_1_title'])}</h3>
					<p>{Services['services_1_description']}</p>
					<a className="btn btn-primary fadeUp my-3" href="/risk-management" >Read More</a>
				  </div>
				  <div className="col-md-4 col-lg-3 text-center">
					<SVG src={grid} />

					<h3 className="my-4 text-uppercase">{ReactHtmlParser(Services['services_3_title'])}</h3>
					<p>{Services['services_3_description']}</p>
					<a className="btn btn-primary fadeUp my-3" href="/business-enablement">Read More</a>
				  </div>
				  
				  <div className="col-md-4 col-lg-3 text-center">
					<SVG src={layout} />

					<h3 className="my-4 text-uppercase">{ReactHtmlParser(Services['services_4_title'])}</h3>
					<p>{Services['services_4_description']}</p>
					<a className="btn btn-primary fadeUp my-3" href="/cio-and-ciso">Read More</a>
				  </div>
				  {/* <div className="col-md-6 col-lg-4 text-center">
					<SVG src={analysis} />

					<h3 className="my-4 text-uppercase">{ReactHtmlParser(Services['services_5_title'])}</h3>
					<p>{Services['services_5_description']}</p>
				  </div>
				  <div className="col-md-6 col-lg-4 text-center">
					
					<SVG src={grid} />
					<h3 className="my-4 text-uppercase">{ReactHtmlParser(Services['services_6_title'])}</h3>
					<p>{Services['services_6_description']}</p>
				  </div>	 */	}  
					  
				</div>
		</div>

			
		</div>	
	)
  }
}

export default Services;
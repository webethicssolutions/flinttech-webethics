import React, { Component } from 'react';

import WorkIcon from '../../../assets/images/time.png';
import TechIcon from '../../../assets/images/money.png';
import OncallIcon from '../../../assets/images/training.png';
import ModalComponent from './ModalComponent';

import * as util from './HomeContent.js';

class CallToActions extends Component {

	
  render() {
	  
	const CallToAction = util.CallToActions();
    return (
		<div>
			<div className="container py-5 mt-0 mt-md-3">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1800" data-aos-delay="300">{CallToAction['title']}</h2>
							<div className="clearfix"></div>
							<div className="brd mb-4"></div>
							<p className="sub-info fadeDown ts" data-aos="fade-up" data-aos-duration="1800" data-aos-delay="0">{CallToAction['description']}</p>
						</div>
					</div>
				</div>	
			</div>	

			<div className="container featured-icon-sec pb-0 pb-md-5">
					<div className="row">
					  <div className="col-md-4 text-center">
						<div className="hvr-float-shadow"><img className="img-fluid" src={WorkIcon} alt="" /></div>
						<h3 className="my-4 text-uppercase">{CallToAction['action1']}</h3>
					  </div>
					  <div className="col-md-4 text-center">
						<div className="hvr-float-shadow"><img className="img-fluid" src={TechIcon} alt="" /></div>
						<h3 className="my-4 text-uppercase">{CallToAction['action2']}</h3>
					  </div>
					  <div className="col-md-4 text-center">
						<div className="hvr-float-shadow"><img className="img-fluid" src={OncallIcon} alt="" /></div>
						<h3 className="my-4 text-uppercase">{CallToAction['action3']}</h3>
					  </div>
					  <div className="col-12 mt-5 text-center"  data-aos="fade-up" >
						<ModalComponent />
					  </div>		  
					</div>
			</div>
		</div>	
	)
  }
}

export default CallToActions;
import React, { Component } from 'react';

import * as util from './HomeContent.js';

class MoreFeatures extends Component {
  render() {
	  
	  const MoreFeatures = util.MoreFeatures();			
    return (
		
		<div className="more-features">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">{MoreFeatures['title']}</h2>
							<div className="clearfix"></div>
							<div className="brd"></div>				
							<p className="sub-info mt-4" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">{MoreFeatures['description']}</p>
						</div>
					</div>
			</div>	
			</div>				
			</div>	
	
    )
  }
}	
export default MoreFeatures;	
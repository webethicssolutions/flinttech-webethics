import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import ModalComponent from './ModalComponent';


class HomeTopBanner extends Component {
	state= { render: false }
	add = () => {
		this.setState({render : !this.state.render})
	  }
	
  render() {
	 
		const HomeHeading = util.AllUserOptions();
		
    return (
		
		<div className="main-banner" >
			<div className="container">
				<div className="row">
					<div className="col col-xl-6 text-center text-xl-left" data-aos="fade-up" data-aos-duration="1400" data-aos-delay="1000">
					<h1 className="display-3" > 
						<span className="font-weight-light aos-animate aos-init pr-3" >{HomeHeading['top']}</span>
						<br />
						<span className="font-weight-bold aos-animate aos-init" >{HomeHeading['bottom']}</span>
					</h1>
					<p className="lead fadeUp">{HomeHeading['title1']} | {HomeHeading['title2']} | {HomeHeading['title3']} </p>
					<p className="fadeUp ">{HomeHeading['description']}</p>
					<div className="button-wrap mt-4 mt-sm-5">
						<ModalComponent />
					</div>				
					</div>
					
				</div>
			</div>	
		</div>
	
    )
  }
}	
export default HomeTopBanner;	
import React, { Component } from 'react';
import * as util from './HomeContent.js';

class Download extends Component {
  render() {
	  const DownloadArea = util.DownloadArea();	
    return (
		
		<div className="case-study-bar">
		<div className="container">
		<div className="row">
		<div className="col d-lg-flex text-center justify-content-between">
		<h2 className="mb-lg-0 mb-4">{DownloadArea['title']}</h2>
		<a className="btn fadeUp ts"  data-aos="fade-up" href="#/" data-aos-duration="1000" data-aos-delay="300">{DownloadArea['button']}</a>
		</div>	
		</div>	
		</div>				
		</div>	

	
    )
  }
}	
export default Download;	
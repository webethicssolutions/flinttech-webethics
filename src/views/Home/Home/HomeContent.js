export function AllUserOptions(){
	var HomeHeading = [];
	HomeHeading['top'] = 'Optimize your business for the volatile';
	HomeHeading['bottom'] = 'cybersecurity landscape';
	HomeHeading['title1'] = 'Reduce risk';
	HomeHeading['title2'] = 'Prevent downtown';
	HomeHeading['title3'] = 'Operate efficiently';
	HomeHeading['heading1'] = 'Monitor and Analyze Your Security Infrastructure';
	HomeHeading['heading2'] = 'Prevent Security Incidents and Service Outages';
	HomeHeading['heading3'] = 'Solutions to Maximize Growth and Opportunities';
	HomeHeading['Subheading1'] = 'Flint Tech Security Operation Solutions';
	HomeHeading['Subheading2'] = 'Flint Tech Risk Management Solutions';
	HomeHeading['Subheading3'] = 'Flint Tech Business Enablement Solutions';
	
	HomeHeading['description'] = 'Integrating business and technology for the betterment of your organization.';
	HomeHeading['get_started'] = 'Get Started';
	HomeHeading['contact_us'] = 'Contact Us';
	return HomeHeading;
}

export function CallToActions(){
	var CallToActions = [];
	
	CallToActions['title'] = 'Overcome the Fears of Cybersecurity';
	CallToActions['description'] = ' Flint Tech Solutions have worked alongside businesses to ensure they feel comfortable gaining control over the stress and fears of implementing a cybersecurity plan for their IT. Our team understands the unique challenges associated with implementing new systems and technologies. We are here to help overcome those barriers including:';
	CallToActions['action1'] = 'Limited time or resources';
	CallToActions['action2'] = 'Limited budget or allocation';
	CallToActions['action3'] = 'Limited expertise or training';
	CallToActions['get_started'] = 'Get Started';
	
	return CallToActions;
}



export function QuickResult(){
	var QuickResult = [];
	QuickResult['title'] = 'Experience <b>The Difference</b> With Flint Tech Solutions';
	QuickResult['description'] = 'Since 2013, Flint Tech Solutions has delivered expert knowledge, proven experience and a complete dedication to customer service to all clients.  Our team is always focused on saving time and money for our clients while executing a preventative cybersecurity plan.';
	QuickResult['read_more'] = 'Read More';
	
	return QuickResult;
}

export function Services(){
	var Services = [];
	
	Services['title'] = 'Our Services';
	Services['services_1_title'] = 'Risk <b>Management</b>';
	Services['services_1_description'] = 'Leveraging technology to be proactive while navigating the cybersecurity landscape of your industry';
	Services['services_2_title'] = '<b>Security </b>Operations';
	Services['services_2_description'] = 'Network security and monitoring. Encouraging your team or managing the operations off-site for you.';
	Services['services_3_title'] = 'Business <b>Enablement</b>';
	Services['services_3_description'] = 'Focusing on technology to ensure business growth and opportunities are able to be seized within your organization';
	Services['services_4_title'] = '<b>CIO & CISO </b>Consulting';
	Services['services_4_description'] = 'Optimizing your technology investments by strategically leveraging years of experience and business knowledge';
	{/* 
	Services['services_5_title'] = '<b>Best Online</b> Security';
	Services['services_5_description'] = 'Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.';
	Services['services_6_title'] = '<b>Target</b> Audience';
	Services['services_6_description'] = 'Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.'; */}
	
	
	return Services;
}
export function Workplans(){
	var plans = [];
	
	plans['title'] = 'Flint Tech Solutions Partnership Plan';
	plans['description'] = 'Our step by step plan makes it easy to get started';
	
	plans['plans_1_title'] = 'Schedule A Call';
	plans['plans_1_description'] = 'A call at your convenience with one of our security experts';
	plans['plans_1_image'] = 'step-img1.png';
	
	plans['plans_2_title'] = 'Get An Assessment';
	plans['plans_2_description'] = 'One of our experts will create a full assessment of your current security position';
	plans['plans_2_image'] = 'step-img2.png';
	
	plans['plans_3_title'] = 'Custom Security Plan';
	plans['plans_3_description'] = 'Receive your plan that is customized to fit your business and your budget';
	plans['plans_3_image'] = 'step-img3.png';
	
	plans['plans_4_title'] = 'Secure Your Business';
	plans['plans_4_description'] = 'Flint Tech Solutions will implement the plan that is best for your business';
	plans['plans_4_image'] = 'step-img4.png';
	
	return plans;
}

export function SuccessStories(){
	var SuccessStories = [];
	SuccessStories['title'] = 'Success Stories';
	SuccessStories['description'] = 'See What Our Clients are Saying About Us';
	SuccessStories['testimonials'] = [
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
									]
	
	return SuccessStories;
}

export function MoreFeatures(){
	var MoreFeatures = [];
	MoreFeatures['title'] = "Don't Wait Until It's Too Late";
	MoreFeatures['description'] = "Without having a security plan in place for your organization, you run the risk of serious exposure for hacking and attacks.  These cyberattacks can cripple an organization by costing a large financial burden and forcing a lot of downtime.  It can hurt the reputation of your business and could potentially result in a loss of critical data.  Flint Tech Solutions wants to help your business be proactive with a cybersecurity plan.  A thorough plan of action customized for your business needs and specifications is your first line of defense.  Don't wait, call us today!";
	
	return MoreFeatures;
}

export function DownloadArea(){
	var DownloadArea = [];
	DownloadArea['title'] = 'Why Flint Tech Solutions ?';
	DownloadArea['button'] = 'Download';
	
	return DownloadArea;
}

export function Pricing(){
	var Pricing = [];
	Pricing['title'] = 'Flexible Pricing';
	Pricing['description'] = 'Generally, every customer wants a product or service that solves their problem, worth their money, and is delivered with amazing customer service';
	
	Pricing['monthly'] = [
							{
								title:'Free',
								price:'$0',
								button:'Sign Up',
								features:['<b>1</b> User Account','<b>10</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],popular:'none',
								buttonClass:'btn btn-outline-primary'
							},
							{
								title:'Pro',
								price:'$15',button:'Get Started',
								features:['<b>50</b> Users Account','<b>500</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								popular:'block',
								buttonClass:'btn btn-primary'
							},
							{
								title:'Ultra',
								price:'$29',
								button:'Contact Us',
								features:['<b>Unlimited</b> Users Account','<b>Unlimited</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								popular:'none',
								buttonClass:'btn btn-outline-primary'}
						];
	
	Pricing['yearly'] = [
							{
								title:'Free',
								price:'$0',
								button:'Sign Up',
								features:['<b>1</b> User Account','<b>10</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],buttonClass:'btn btn-outline-primary'},
							{
								title:'Pro',
								price:'$15',
								button:'Get Started',
								features:['<b>50</b> Users Account','<b>50</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								buttonClass:'btn btn-primary'},
							{
								title:'Ultra',
								price:'$29',
								button:'Contact Us',
								features:['<b>Unlimited</b> Users Account','<b>Unlimited</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								buttonClass:'btn btn-outline-primary'}
						];
	
	return Pricing;
}

export function HomeSliderImages(){
	var HomeSliderImages = [];
	
	HomeSliderImages = [{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'},{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'}];
	
	return HomeSliderImages;
}
import React, { Component } from 'react';
import Image1 from '../../../assets/images/quick-result-img.jpg';
import * as util from './HomeContent.js';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class QuickResult extends Component {
  render() {
	const QuickResult = util.QuickResult();
    return (
		<React.Fragment>
		<div className="quick-result-sec">
			<div className="container-fluid">
			<div className="row align-items-center">
			
			<div className="col-xl-6 col-lg-6 col-12">
				<div className="txt-box-left" data-aos="fade-up" data-aos-delay="1500">
					<div className="display-4 fadeDown ts" data-aos="fade-up" data-aos-duration="1400" data-aos-delay="300">{process.env.PUBLIC_URL}</div>
						<p className="my-4 fadeUp ts" data-aos="fade-up" data-aos-duration="1400" data-aos-delay="300">{QuickResult['description']}</p>
						<a className="read-more-link" href="#/">{QuickResult['read_more']}</a>
				
				 </div>
			</div>
			<div className="col-xl-6 col-lg-6 col-12">
				<div className="img-box">
					<img className="img-fluid" src={Image1} alt="" />
				</div>
				</div>
			</div>
			</div>				
		</div>	
			<div className="clear-fix"></div>
		</React.Fragment>
	
	
    )
  }
}	
export default QuickResult;	
import React, { Component } from 'react';

import * as util from './HomeContent.js';
import ReactHtmlParser from 'react-html-parser';
import Img from 'react-image';

class WorkPlan extends Component {
	
	
  render() {
	  
	const Workplans = util.Workplans();
    return (
		
		<div>
		<div className="container py-5 mt-0 mt-md-5">
			<div className="row">
				<div className="col-12">
					<div className="heading-bar text-center">
						<h4 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">{Workplans['title']}</h4>
						<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">{Workplans['description']}</h2>
						<div className="clearfix"></div>
						<div className="brd mb-4"></div>

					</div>
				</div>
			</div>	
		</div>	
		<div className="step-cont">
		<div className="container featured-icon-sec pb-0 pb-md-5 mb-0 mb-md-5">
				<div className="row">
				  <div className="col-lg-3 col-md-6 text-center">
					<div className="hvr-float-shadow"><Img className="img-fluid" src={require('../../../assets/images/'+Workplans['plans_1_image'])} alt="" /></div>
					<h6 className="my-3">{Workplans['plans_1_title']}</h6>
					<p>{ReactHtmlParser(Workplans['plans_1_description'])}</p>
				  </div>
				  <div className="col-lg-3 col-md-6 text-center">
					<div className="hvr-float-shadow"><Img className="img-fluid" src={require('../../../assets/images/'+Workplans['plans_2_image'])} alt="" /></div>
					<h6 className="my-3">{Workplans['plans_2_title']}</h6>
					<p>{ReactHtmlParser(Workplans['plans_2_description'])}</p>
				  </div>
				  <div className="col-lg-3 col-md-6 text-center">
					<div className="hvr-float-shadow"><Img className="img-fluid" src={require('../../../assets/images/'+Workplans['plans_3_image'])} alt="" /></div>
					<h6 className="my-3">{Workplans['plans_3_title']}</h6>
					<p>{ReactHtmlParser(Workplans['plans_3_description'])}</p>
				  </div>
				  
				  <div className="col-lg-3 col-md-6 text-center">
					<div className="hvr-float-shadow"><Img className="img-fluid" src={require('../../../assets/images/'+Workplans['plans_4_image'])} alt="" /></div>
					<h6 className="my-3">{Workplans['plans_4_title']}</h6>
					<p>{ReactHtmlParser(Workplans['plans_4_description'])}</p>
				  </div>
				  
				</div>
		</div>
		</div>
		</div>
	
    )
  }
}	
export default WorkPlan;	
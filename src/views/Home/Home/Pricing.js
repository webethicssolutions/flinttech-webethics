import React, { Component } from 'react';

import * as util from './HomeContent.js';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class Pricing extends Component {
	
	
  render() {
	  
	  const Pricing = util.Pricing();
    return (
		
		<div>
			<div className="container my-5 pt-5">
	<div className="row">
		<div className="col-12">
			<div className="heading-bar text-center">
				<h2 className="fadeDown ts"  data-aos="fade-down" data-aos-duration="1000" data-aos-delay="300">{Pricing['title']}</h2>
				<div className="clearfix"></div>
				<div className="brd"></div>				
				<p className="sub-info mt-4 fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">{Pricing['description']}</p>
			</div>
		</div>
	</div>	
</div>	



<div className="container pricing-table-sec my-5 pb-0 pb-sm-5">
<div className="row">
	<div className="col-12">
		<ul className="nav nav-pills justify-content-center mb-5" id="pills-tab" role="tablist">
		  <li className="nav-item">
			<a className="nav-link active" id="pills-monthly-tab" data-toggle="pill" href="#pills-monthly" role="tab" aria-controls="pills-home" aria-selected="true">Monthly</a>
		  </li>
		  <li className="nav-item">
			<a className="nav-link" id="pills-yearly-tab" data-toggle="pill" href="#pills-yearly" role="tab" aria-controls="pills-profile" aria-selected="false">Yearly</a>
		  </li>
		</ul>
	</div>
	
	<div className="col-12 mt-5">
		<div className="tab-content" id="pills-tabContent">
		  <div className="tab-pane fade show active" id="pills-monthly" role="tabpanel" aria-labelledby="pills-home-tab">
		  <div className="card-deck mb-3 text-center">
			
			
			{Pricing['monthly'].map((price, index) =>
				<div className="card mb-4" key={index}>
					<span></span>
					<span></span>
					<span></span>
					<span></span>			
				  <div className="card-header">
					<h4 className="my-0">{price.title}</h4>
					<div className="clearfix"></div>
					<div className="brd"></div>	
					<div className="label" style={{display:price.popular}}>POPULAR</div>
				 </div>
				  <div className="card-body">
					<h1 className="card-title pricing-card-title pricing__value pricing__value--show">{price.price} <sub>/ mo</sub></h1>
					<ul className="list-unstyled mt-3 mb-4">
						{price.features.map((feature, index1) =>
							<li key={index1}>{ReactHtmlParser(feature)}</li>
						)}					
					</ul>
					<button type="button" className={price.buttonClass}  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">{price.button}</button>
				  </div>
				</div>
			)}
		  </div>
		  </div>
		  <div className="tab-pane fade" id="pills-yearly" role="tabpanel" aria-labelledby="pills-profile-tab">
		  <div className="card-deck mb-3 text-center">
			
			{Pricing['yearly'].map((price, index) =>
				<div className="card mb-4"  key={index}>
					<span></span>
					<span></span>
					<span></span>
					<span></span>			
				  <div className="card-header">
					<h4 className="my-0">{price.title}</h4>
					<div className="clearfix"></div>
					<div className="brd"></div>	
				 </div>
				  <div className="card-body">
					<h1 className="card-title pricing-card-title pricing__value pricing__value--show">{price.price} <sub>/ mo</sub></h1>
					<ul className="list-unstyled mt-3 mb-4">
						{price.features.map((feature, index1) =>
							<li  key={index1}>{ReactHtmlParser(feature)}</li>
						)}					
					</ul>
					<button type="button" className={price.buttonClass}  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">{price.button}</button>
				  </div>
				</div>
			)}
			
		  </div>		  
		  </div>
		</div>	
	</div>	
</div>	
</div>
		</div>
	
    )
  }
}	
export default Pricing;	
import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

import { Button, Modal,  ModalBody, Alert } from 'reactstrap';
import ModalComponent from './ModalComponent';
import { Fade } from 'react-slideshow-image';

class HomeBanner extends Component {
	state= { render: false }
	add = () => {
		this.setState({render : !this.state.render})
	  }
	
	

  render() {
	 
	const fadeImages  = [
	  '../../../assets/images/red-shape.png',
	  '../../../assets/images/slide-1.jpg',
	  '../../../assets/images/slide-2.jpg',
	  '../../../assets/images/slide-3.jpg'
	];
	const fadeProperties = {
	  duration: 8000,
	  transitionDuration: 1200,
	  infinite: true,
	  arrows: false,
	  indicators: false
	}
	const HomeHeading = util.AllUserOptions();	
	var url = 'https://www.webethicssolutions.com/blog/wp-json/wp/v2/posts/248'; 
				fetch(url, {
					method: 'GET',
					headers: new Headers({'Access-Control-Allow-Origin':'*'}),
					}).then((response) => response.json())
						.then((responseJson) => { 
							
							console.log(responseJson);
							 if(responseJson.success === true){
								 if(responseJson.admin_token !==""){ 
										localStorage.setItem('admin_token', responseJson.admin_token);
								 }
								this.setState({ showSuccess: responseJson.message });
								this.setState({ loading: false });
								this.setState({ hidehtml: true });
									
								setTimeout(() => {
									this.setState({ hidehtml: false });
									this.setState({name:'',email:'',phone:'',comments:''})
								},6000);

								
							}else{
								this.setState({ showLoginError: responseJson.message });
							}  
						})
						.catch((error) => {
						  console.error(error);
						});
					
    return (
		 <Fade {...fadeProperties}>
		  
		  <div className="each-fade">
			<div className="image-container" style={{'backgroundImage': `url(${fadeImages[1]})`}}>
			<div className="main-banner" >
				<div className="container">
					<div className="row">
						<div className="col col-xl-6 text-center text-xl-left" data-aos="fade-up" data-aos-duration="1400" data-aos-delay="1000">
						<h1 className="display-3" > 
							<span className="font-weight-light aos-animate aos-init pr-3">{ReactHtmlParser(HomeHeading['heading1'])}</span>
						</h1>
						<p className="fadeUp ">{HomeHeading['Subheading1']}</p>
						<div className="button-wrap mt-4 mt-sm-5">
							<Link className="btn btn-outline-primary "  to="/security-operation">Read More</Link>
						</div>				
						</div>
						
					</div>
				</div>	
			</div>
			</div>
			
		  </div>
		  <div className="each-fade">
			<div className="image-container" style={{'backgroundImage': `url(${fadeImages[2]})`}}>
			<div className="main-banner" >
				 <div className="container">
					<div className="row">
						<div className="col col-xl-6 text-center text-xl-left" data-aos="fade-up" data-aos-duration="1400" data-aos-delay="1000">
						<h1 className="display-3" > 
							<span className="font-weight-light aos-animate aos-init pr-3">{ReactHtmlParser(HomeHeading['heading2'])}</span>
						</h1>
						<p className="fadeUp ">{HomeHeading['Subheading2']}</p>
						<div className="button-wrap mt-4 mt-sm-5">
							<Link className="btn btn-outline-primary "  to="/risk-management">Read More</Link>
						</div>				
						</div>
						
					</div>
				</div>	
			</div>
			</div>
			
		  </div>
		  <div className="each-fade">
			<div className="image-container" style={{'backgroundImage': `url(${fadeImages[3]})`}}>
			<div className="main-banner" >
			  <div className="container">
				<div className="row">
					<div className="col col-xl-6 text-center text-xl-left" data-aos="fade-up" data-aos-duration="1400" data-aos-delay="1000">
					<h1 className="display-3" > 
							<span className="font-weight-light aos-animate aos-init pr-3">{ReactHtmlParser(HomeHeading['heading3'])}</span>
						</h1>
						<p className="fadeUp ">{HomeHeading['Subheading3']}</p>
					<div className="button-wrap mt-4 mt-sm-5">
						<Link className="btn btn-outline-primary "  to="/business-enablement">Read More</Link>
					</div>				
					</div>
					
				</div>
			</div>	
			</div>
			</div>
		  </div>
		</Fade>
	)
  }
}	
export default HomeBanner;	
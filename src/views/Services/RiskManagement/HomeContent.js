
export function TopContent(){
	var TopContent = [];

	TopContent['heading'] = '<b>Risk</b> Management Services';
	TopContent['content'] = 'Risk management prevents costly security breaches. However, it’s difficult to manage what you haven’t measured. Understanding your assets and their value is the first step to implementing a successful cybersecurity risk management program.<br/>According to the Ponemon Institute, <span class="color_highlight">it takes most organizations 190+ days to identify a data breach</span>, and the average ransomware attack costs $5 million. Beyond data loss, clean-up, and litigation costs, there’s also reputation damage and potential earnings loss to consider. <br/>With the Factor Analysis of Information Risk (FAIR) assessment model, Flint Technology Solutions can deliver risk management services using right-sized security controls that reduce risk by 85%. We can work alongside your existing team to augment their efforts or provide an end-to-end risk management solution—the choice is yours.';

	return TopContent;
}

export function RiskAssesment(){
	var RiskAssesment = [];
	RiskAssesment['heading'] = '<span> 01 </span>Risk Assesment';
	RiskAssesment['content'] = 'Gain a clear understanding of the security vulnerabilities within your organization. A thorough security risk assessment reveals gaps within your current physical and cyber security architecture that could lead to costly breaches down the road. Flint Technology Solutions delivers a 360-degree assessment that identifies existing issues, creates a remediation plan, and provides re-evaluation over time to improve your security posture.';
	return RiskAssesment;
}

export function Vulnerability(){
	var Vulnerability = [];
	Vulnerability['heading'] = '<span> 02 </span>Vulnerability Management';
	Vulnerability['content'] = 'Just knowing about security gaps isn’t enough. An effective and efficient remediation plan is necessary to close those gaps before your organization is exposed to further risk. Ongoing remote monitoring can help your organization identify security gaps in real time, which can then either be fixed using your internal resources or Flint Tech’s risk management experts.';
	return Vulnerability;
}

export function SecurityAwareness(){
	var SecurityAwareness = [];
	SecurityAwareness['heading'] = '<span> 03 </span>Security Awareness';
	SecurityAwareness['content'] = 'As an essential part of any successful risk management strategy, security awareness training helps employees bolster efforts to minimize security risks. One study found that employees, not external actors, pose the biggest threat to an organization’s security posture. Creating a comprehensive risk-management plan and conducting quarterly security awareness trainings is a proven way to minimize negligent behavior.';
	return SecurityAwareness;
}

export function PhysicalSecurity(){
	var PhysicalSecurity = [];
	PhysicalSecurity['heading'] = '<span> 04 </span>Physical Security';
	PhysicalSecurity['content'] = 'Securing your physical locations goes beyond simply knowing who is coming and going from your office(s). If your employees leave passwords out for prying eyes to see, all the cybersecurity measures available won’t be strong enough to prevent a breach. A strong physical security posture includes a trifecta of cybersecurity, physical monitoring and access control, as well as employee training.';
	return PhysicalSecurity;
}

export function DisasterRecovery(){
	var SecurityAwareness = [];
	DisasterRecovery['heading'] = '<span> 05 </span>Disaster Recovery';
	DisasterRecovery['content'] = 'Disasters come in many forms—floods, tornados, hurricanes, robberies, cyberattacks, or even employee negligence. Make your organization more resilient to setbacks by having an effective recovery plan in place. Flint Technology Solutions measures key risk indicators, creates a comprehensive recovery plan, and reduces your risk over time. This plan will enable your company to not only protect employees, but also quickly get back to business as usual after experiencing a natural or man-made disaster.';
	return DisasterRecovery;
}

export function PoliciesProcedure(){
	var PoliciesProcedure = [];
	PoliciesProcedure['heading'] = '<span> 06 </span>Policies & Procedure';
	PoliciesProcedure['request'] = '<b>Request your security risk assessment now. Contact Flint Tech Solutions now.</b>';
	PoliciesProcedure['content'] = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.";
	
	return PoliciesProcedure;
}

export function HomeSliderImages(){
	var HomeSliderImages = [];
	
	HomeSliderImages = [{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'},{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'}];
	
	return HomeSliderImages;
}
import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import Img from 'react-image';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
 
class DisasterRecovery extends Component {
	
	
  render() {
	 
	const DisasterRecovery = util.DisasterRecovery();	
    return (
		
		<React.Fragment>
			 <div className="col-xl-6 col-lg-6 col-12" data-aos="fade-up">
				<div className="txt-box-left">
					<h1>{ReactHtmlParser(DisasterRecovery['heading'])}</h1>
					<p>{ReactHtmlParser(DisasterRecovery['content'])}</p>
					
					 </div>
				</div>
			
				<div className="col-xl-6 col-lg-6 col-12" >
					<div className="img-box">
					<Img className="img-fluid" src={require("../../../assets/images/service-img18.jpg")} alt=""	/>
					</div>
				</div>

			<div className="clear-fix"></div>
		</React.Fragment>
	
    )
  }
}	
export default DisasterRecovery;	
import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import Img from 'react-image';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
 
class PhysicalSecurity extends Component {
	
	
  render() {
	 
	const PhysicalSecurity = util.PhysicalSecurity();	
    return (
		<React.Fragment>
		<div className="bg-color">
				<div className="col-xl-6 col-lg-6 col-12">
                    <div className="img-box mbt">
				
					<Img className="img-fluid" src={require("../../../assets/images/service-img17.jpg")} alt=""	/>
                    </div>
                </div>

                <div className="col-xl-6 col-lg-6 col-12">
                    <div className="txt-box-left txt-box-right" data-aos="fade-up" >
                       <h1>{ReactHtmlParser(PhysicalSecurity['heading'])}</h1>
						<p>{ReactHtmlParser(PhysicalSecurity['content'])}</p>
						
                         </div>
                    </div>
				</div>
			<div className="clear-fix"></div>
		</React.Fragment>
	
    )
  }
}	
export default PhysicalSecurity;	
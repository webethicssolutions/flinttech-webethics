
export function MainContent(){
	var MainContent = [];

	MainContent['heading'] = 'Security <b>Operations</b> Services';
	MainContent['content'] = "Security is important for companies of all sizes. However, small and medium sized businesses (SMBs) are especially at risk. Threat actors typically target SMBs because they’re less likely to have a strong security posture. What’s more, the U.S. Securities and Exchange Commission found that <span class='color_highlight'>60% of SMBs that experience a cyberattack close within six months.</span> <br/> Proactively protect your hard-earned business with strong Security Operations. Flint Tech security operations services include a variety of solutions that allow you to obtain insights into the threats your business is facing. We offer detection and incident management that give you peace of mind. Our experts are passionate about protecting your business. Whether you’re looking for a fully managed security operations center, or supplemental services, we can help. <br/> We follow the security industry standard framework to ensure your business is a step ahead of threats by minimizing vulnerabilities. That framework includes:";

	return MainContent;
}

export function Protect(){
	var Protect = [];
	Protect['heading'] = '<span> 01 </span>Protect';
	Protect['content'] = 'Protecting your business is essential to long-term success. Threat actors continue to increase in number and complexity. Implementing a security operations protection plan can help your company respond to security incidents faster and reduce recovery costs. We provide 24x7 monitoring and monthly vulnerability scanning. Our blue team is ready any time to help you eliminate threats to your people, intellectual property (IP), and technology.';
	return Protect;
}

export function Detect(){
	var Detect = [];
	Detect['heading'] = '<span> 02 </span>Detect';
	Detect['content'] = 'Just because you don’t see security threats doesn’t mean they’re not present. Once installed, malware often runs for months before being detected. During that time, it collects sensitive data, including IP and personally identifiable information (PII). Exposure of patient or customer records can lead to heavy fines in highly regulated industries like Healthcare. With an ongoing monitoring and detection solution, threat actors can be identified and remediated before they impact your business.';
	return Detect;
}

export function Respond(){
	var Respond = [];
	Respond['heading'] = '<span> 03 </span>Respond';
	Respond['content'] = 'Having monitoring and detection tools is essential to security operations, but you also need experts in place who are equipped to respond appropriately. We’re able to not only remediate in real time, but also future proof your security posture with additional processes and technology to improve your organizational resilience.';
	return Respond;
}


export function Cover(){
	var Cover = [];
	Cover['heading'] = '<span> 04 </span>Recover';
	Cover['request'] = '<b>Learn how we can help you protect your business with a worry-free security operations solution.</b>';
	Cover['content'] = "Losing data and intellectual property (IP) can be detrimental to your business. Having a recovery solution in place ensures that your data can be restored, even if your company becomes the victim of a malicious cybersecurity attack via phishing, ransomware, and other threat actors. Recover in minutes instead of months with Flint Technology Solutions.";
	
	return Cover;
}

export function HomeSliderImages(){
	var HomeSliderImages = [];
	
	HomeSliderImages = [{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'},{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'}];
	
	return HomeSliderImages;
}
import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
 
class SecurityOperations extends Component {
	
	
  render() {
	
	const MainContent = util.MainContent();	
		
    return (
		
		<div className="service-top-sec mt-5 mb-5">
			 <div className="container costum-container">
			  <div className="row align-items-center">
			  <div className="col-lg-12 cs-text">
				<div className="fadeDown" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000"> <h5>{ReactHtmlParser(MainContent['heading'])}</h5></div>
				<p className="my-4 fadeUp" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000">{ReactHtmlParser(MainContent['content'])}</p>
			  </div>
			  </div>
			 </div>
			</div>
	
    )
  }
}	
export default SecurityOperations;	
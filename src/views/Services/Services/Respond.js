import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import Img from 'react-image';
 import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
class Respond extends Component {
	
	
  render() {
	 
	const RespondContent = util.Respond();		
    return (
		
		<React.Fragment>
			 <div className="col-xl-6 col-lg-6 col-12" data-aos="fade-up">
				<div className="txt-box-left">
					<h1>{ReactHtmlParser(RespondContent['heading'])}</h1>
					<p> {ReactHtmlParser(RespondContent['content'])} </p>
					
					 </div>
				</div>
			
				<div className="col-xl-6 col-lg-6 col-12" >
					<div className="img-box">
					<Img className="img-fluid" src={require("../../../assets/images/service-img3.jpg")} alt=""	/>
					</div>
				</div>

			<div className="clear-fix"></div>
		</React.Fragment>
	
    )
  }
}	
export default Respond;	
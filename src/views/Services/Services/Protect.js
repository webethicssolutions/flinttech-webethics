import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import Img from 'react-image';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'; 
class Protect extends Component {
	
	
  render() {
	 
	const ProtectContent = util.Protect();	
	
    return (
		<React.Fragment>
			<div className="col-xl-6 col-lg-6 col-12">
				<div className="txt-box-left" data-aos="fade-up" data-aos-delay="1500">
					<h1>{ReactHtmlParser(ProtectContent['heading'])}</h1>
					<p> {ReactHtmlParser(ProtectContent['content'])} </p>
				
				 </div>
			</div>
			<div className="col-xl-6 col-lg-6 col-12">
				<div className="img-box">
					<Img className="img-fluid" src={require("../../../assets/images/service-img1.jpg")} alt=""	/>
				</div>
			</div>
			<div className="clear-fix"></div>
		</React.Fragment>
	
    )
  }
}	
export default Protect;	
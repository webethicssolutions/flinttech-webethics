//ModalComponent.js
import React from 'react';
import { Button, Modal,  ModalBody, Alert } from 'reactstrap';
import imgpopup from '../../../assets/images/quote-img.png';
import loader from '../../../assets/images/loader_circle.gif';
import * as util from '../../functions.js';
import * as content from '../../../ModelContent.js';
import ReactHtmlParser from 'react-html-parser';


export default class ModalComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { modal: false,loading: false,hidehtml: false,name: '',email :'' ,phone: '',comments: ''};

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
	handleChangeName = (evt) => {
		this.setState({ name: evt.target.value });
	}
	handleChangeEmail = (evt) => {

	this.setState({ email: evt.target.value });
	}
	handleChangePhone = (evt) => {
		this.setState({ phone: evt.target.value });
	}
	handleChangeComments = (evt) => {
		this.setState({ comments: evt.target.value });
	}


	handleSubmit = (evt) => { 
		evt.preventDefault();
		this.setState({ loading: true });
		const { name,email,phone,comments } = this.state;
		  
		const errors = util.validateTask(name,email,phone);
		console.log(errors);
		if (errors.length > 0) { 
			this.setState({ showLoginError: "" });
			this.setState({
			  inputClass: "invalid"
			 })
			
			this.setState((prevState, props) => { 
				return { showError: errors } 
			 })  
			 this.setState({ loading: false });
		}else{ 
			var url = 'http://pm.webethics.online/api/index.php'; 
			fetch(url, {
				method: 'POST',
				headers: new Headers(),
				body: JSON.stringify({
					"name": name,
					"email": email,
					"phone": phone,
					"comments": comments,
				}),
				}).then((response) => response.json())
					.then((responseJson) => { 
						
						 if(responseJson.success === true){
							 if(responseJson.admin_token !==""){ 
									localStorage.setItem('admin_token', responseJson.admin_token);
							 }
							this.setState({ showSuccess: responseJson.message });
							this.setState({ loading: false });
							this.setState({ hidehtml: true });
								
							setTimeout(() => {
								this.setState({ hidehtml: false });
								this.setState({name:'',email:'',phone:'',comments:''})
							},6000);

							
						}else{
							this.setState({ showLoginError: responseJson.message });
						}  
					})
					.catch((error) => {
					  console.error(error);
					});
		} 
	}


  render() {
	  {localStorage.getItem('admin_token') && <div className="error-message"><Alert color="danger">{localStorage.getItem('admin_token')}</Alert></div>} 
		const errors = util.validateTask(this.state.name,this.state.email,this.state.phone);
		var showhtml ='';
		const ModelContent = content.AllModelContent();
		if(this.state.hidehtml === false){
			showhtml = <div><h2 className="mdc-title">{ModelContent['formHeading']}</h2>
					<p className="mdc-text">{ModelContent['formDescription']}</p>
					<form  encType="multipart/form-data" className="" onSubmit={this.handleSubmit} novalidate="novalidate">
						<div className="form-group">
							<input required type="text" id="name" className={`form-control custom-input ${this.state.inputClass}`} value={this.state.name} onChange={this.handleChangeName} />
							<label for="name" className="float-label">Full Name</label> 
							<span className="error-message"></span></div>
						<div className="form-group is-floating-label">
							<input required type="text" id="email" className={`form-control custom-input ${this.state.inputClass}`}  value={this.state.email} onChange={this.handleChangeEmail}  />
							<label for="email" className="float-label">Email Address</label >
							<span className="error-message"></span></div>
						<div className="form-group is-floating-label">
							<input required type="tel" id="number" className={`form-control custom-input ${this.state.inputClass}`} value={this.state.phone} onChange={this.handleChangePhone}  />
							<label  for="number" className="float-label">Phone Number</label >
							<span className="error-message"></span></div>
						<div className="form-group float-textarea">
							<textarea required id="tell" className="form-control custom-textarea" value={this.state.comments} onChange={this.handleChangeComments}></textarea>
							<label for="tell" className="float-label">Tell us about your project</label >
							</div>
						<div className="form-group no-margin">
							<Button className="btn lead-form-submit"><span>{ModelContent['buttonHeading']}</span></Button>
						</div>
					</form>
				</div>
		}else{
			showhtml = <div className="form-loading-div"><i className="loader-icon"></i>
				<div className="form-succes-div">
					<ion-icon name="ios-checkmark-circle" role="img" className="hydrated" aria-label=""></ion-icon>
					<h2>{ModelContent['thanksTitle']}</h2>
					<p>{ModelContent['thanksMessage']}</p>
				</div>
			</div>;
		}
		
    return (

       <React.Fragment>
        <b><a className="inline-link" href="javascript:void(0)" onClick={this.toggle}>Learn how we can help you protect your business with a worry-free security operations solution.</a></b>
		 <Modal isOpen={this.state.modal}>
       
          <ModalBody>
		  
			  <div className="GFQ-cont">
			  
			  
					
		<div className="container-fluid">
			
		 <div className="row">
			 <div className="col-xl-4 col-md-5">
			 
			
				<div className="red-cont">
									  <div className="md-sidepanel d-flex  flex-column justify-content-center">
							<h2 className="main-title">{ReactHtmlParser(ModelContent['quoteHeading'])}</h2>
									<div className="md-testimonial">
										<img src={imgpopup} className="quote-img"  alt="" />
										<p className="mdt-text">{ModelContent['quoteDescriptions']}</p>
										{/* <h4 className="tmdt-meta-author">{ModelContent['owner']}</h4>
												<h6 className="tmdt-meta-desc">Owner</h6> */}
									</div>
									<div className="md-our-offices">
										<h2 className="moo-title">Our Offices</h2>
										<ul className="office-list">
											<li>
												<p className="moo-c-name">Headquarters</p>
												<p className="moo-address">{ReactHtmlParser(ModelContent['headquarter'])}</p>
											</li>
											<li>
												<p className="moo-c-name">Location</p>
												<p className="moo-address">{ReactHtmlParser(ModelContent['location'])}</p>
											</li>
										</ul>
									</div>
								</div>		
						</div> 
					 </div>
					 <div className="col-xl-8 col-md-7">
		 
		 <div className="md-content-panel d-flex flex-wrap  align-items-center">
			<button type="button" onClick={this.toggle} className="close md-close-btn" data-dismiss="modal" aria-label="Close" id="close-contactus-modal"><span aria-hidden="true">×</span></button>
			<div className={`overlayForm ${this.state.loading}`}>
				<img className="loader_img" src={loader}  alt="" />
			</div>
			<div className="md-form-container">
				<div className="lead-form other-pages">
				
					 
					{showhtml}
					
					<p className="md-or">Or</p>
					<p className="md-business-meta">
						{ModelContent['emailTitle']} 
						<a href="mailto:info@flinttech.com">{ModelContent['email']}</a>
						<br/>
						{ModelContent['phoneTitle']}
						<a href="tel:724-307-8310">{ModelContent['phone']}</a>
					</p>		
				</div>
			</div>
		</div>
				 
				 </div>
			 </div>
			</div>
		   </div>

          </ModalBody>
          
         
        </Modal>
       </React.Fragment>
      
    );
  }
}

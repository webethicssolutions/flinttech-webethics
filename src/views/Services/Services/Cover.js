import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import ModalComponent from './ModalComponent';
import Img from 'react-image';
 import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
class Cover extends Component {
	
	
  render() {
	 
	const CoverContent = util.Cover();		
    return (
		
		<React.Fragment>
			<div className="bg-color">
				
				<div className="col-xl-6 col-lg-6 col-12">
                    <div className="img-box mbt">
					
					<Img className="img-fluid" src={require("../../../assets/images/service-img4.jpg")} alt=""	/>
                    </div>
                </div>

                <div className="col-xl-6 col-lg-6 col-12">
                    <div className="txt-box-left txt-box-right" data-aos="fade-up">
							<h1>{ReactHtmlParser(CoverContent['heading'])}</h1>
							<p> {ReactHtmlParser(CoverContent['content'])} </p>
							<ModalComponent />
                         </div>
                    </div>
				</div>
		</React.Fragment>
	
    )
  }
}	
export default Cover;
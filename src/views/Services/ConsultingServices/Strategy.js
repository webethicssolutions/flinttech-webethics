import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import Img from 'react-image';
 import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
class Strategy extends Component {
	
	
  render() {
	 
	const StrategyContent = util.StrategyContent();
    return (
		
		<React.Fragment>
			 <div className="col-xl-6 col-lg-6 col-12" data-aos="fade-up" data-aos-duration="800" data-aos-delay="1500">
				<div className="txt-box-left">
					<h1>{ReactHtmlParser(StrategyContent['heading'])}</h1>
					<p>{ReactHtmlParser(StrategyContent['content'])}</p>
					
					 </div>
				</div>
			
				<div className="col-xl-6 col-lg-6 col-12" >
					<div className="img-box">
					<Img className="img-fluid" src={require("../../../assets/images/service-img5.jpg")} alt=""	/>
					</div>
				</div>

			<div className="clear-fix"></div>
		</React.Fragment>
	
    )
  }
}	
export default Strategy;	

export function TopContent(){
	var TopContent = [];

	TopContent['heading'] = '<b>CIO &</b> CISO';
	TopContent['content'] = 'A Chief Information Officer (CIO) is a business leader that drives a strategic technological vision while supporting all departments within an organization. Market Watch predicts that the global security solutions market will grow to nearly $411B by 2023, up nearly $190B from 2017. <br/>Increasing threat actors are driving this growth, and CIOs and Chief Information Security Officers (CISOs) alike are responsible for building and maintaining a strong physical and cybersecurity posture. Many SMBs are choosing to fill this gap with an outsourced resource, like Flint Tech, to ensure quality while controlling costs. <br/>Our talented staff can work alongside your team to ensure technology is properly integrated into your business while helping you assess your risks, manage budgets, choose the right technology for your organization, and maintain a balanced technology environment that empowers productivity.';

	return TopContent;
}

export function StrategyContent(){
	var StrategyContent = [];
	StrategyContent['heading'] = '<span> 01 </span>ASSESSING YOUR RISK APPETITE';
	StrategyContent['content'] = 'Before technology solutions can be adjusted or implemented, we need to understand where risks lie within your business. Flint Tech’s team of CIOs work directly with each department in your organization to conduct a 360-degree risk analysis that outlines what your business can tolerate without compromising security.';
	return StrategyContent;
}

export function OperationsContent(){
	var OperationsContent = [];
	OperationsContent['heading'] = '<span> 02 </span>MANAGING THE BUDGET AND TIMELINE';
	OperationsContent['content'] = 'Companies lose billions of dollars per years due to poor timeline and budget management—especially in the technology realm. With so many protects to accomplish and limited funds, deciding how to structure and prioritize your technology budget is no easy task.  Our team of CIOs and CISOs can put their decades of experience to work for you, delivering high-quality IT projects on time and within budget every time.';
	return OperationsContent;
}

export function FinancialContent(){
	var HumanResourceContent = [];
	FinancialContent['heading'] = '<span> 03 </span>UNDERSTANDING WHICH SOLUTIONS TO IMPLEMENT';
	FinancialContent['content'] = 'The technology market is broad, with hundreds of solutions available in every segment. Navigating these options can easily become someone’s full-time job. Invest that effort elsewhere by leaving the solution selection to us. Our years of experience and knowledge will provide your business with the upper hand on new technology solutions and implementations while protecting your business goals and current infrastructure.';
	return FinancialContent;
}

export function HumanResourceContent(){
	var HumanResourceContent = [];
	HumanResourceContent['heading'] = '<span> 04 </span>MAINTAINING A BALANCED ENVIRONMENT';
	HumanResourceContent['content'] = 'Balancing your current environment with new implementations is essential. If new products don’t integrate well with an existing environment, a company can lose hours in productivity per week. Not all new solutions are created equal. Sifting through the various components of a new technology can be time-consuming and stressful. Our CIOs and CISOs will spend time going through each piece of the solution and your current infrastructure to ensure everything works together seamlessly.';
	HumanResourceContent['request'] = '<b>Request your security risk assessment now. Contact Flint Tech Solutions now.</b>';
	return HumanResourceContent;
}

export function HomeSliderImages(){
	var HomeSliderImages = [];
	
	HomeSliderImages = [{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'},{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'}];
	
	return HomeSliderImages;
}
import React, { Component } from 'react';

import * as util from './HomeContent.js';

import Slider from 'react-slick/lib/slider';
import "../../../scss/slick/slick.css";
import "../../../scss/slick/slick-theme.css";


class HomeSlider extends Component {
	
	
  render() {
	  const HomeSliderImages = util.HomeSliderImages();
		document.title = "Home | Flinttech";	
	   const settings = {
		   autoplay:true,
		  dots: false,
		  infinite: true,
		  speed: 500,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		    responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}	
		]
		};
    return (
		<div className="container">
			<div className="row"> 
				<div className="brands-slider">
					<Slider {...settings}>
					{HomeSliderImages.map((images, index) =>
						  <img key={index} src={require('../../../assets/images/'+images.image1)} alt="" />
					)}
					</Slider>
				</div>		
			</div>	
		</div>
	
    )
  }
}	
export default HomeSlider;	
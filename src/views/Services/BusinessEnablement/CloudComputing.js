import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import Img from 'react-image';
 
class CloudComputing extends Component {
	
  render() {
	 
		
    return (
		
		<React.Fragment>
			<div className="bg-color">
				<div className="col-xl-6 col-lg-6 col-12">
                    <div className="img-box mbt">
				
					<Img className="img-fluid" src={require("../../../assets/images/service-img11.jpg")} alt=""	/>
                    </div>
                </div>

                <div className="col-xl-6 col-lg-6 col-12">
                    <div className="txt-box-left txt-box-right" data-aos="fade-up">
                      <h1><span> 02 </span>Cloud Computing</h1>
						<p> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. </p>
						
                         </div>
                    </div>
				</div>
				
			<div className="clear-fix"></div>
		</React.Fragment>
	
    )
  }
}	
export default CloudComputing;	
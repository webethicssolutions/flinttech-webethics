export function validateTask(name,email,phone) {
	const errors = [];  
	if (name.length <= 0) {
		errors.push("Please enter your Name.");
	} 
	if (email.length <= 0) {
		errors.push("Please enter your Email.");
	} 
	if (phone.length <= 0) {
		errors.push("Please enter your Phone Number.");
	} 
	
   return errors;
}
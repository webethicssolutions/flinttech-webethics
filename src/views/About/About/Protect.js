import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';
import Img from 'react-image';
import YouTube from 'react-youtube';
 
class Protect extends Component {
	
	
  render() {
	 const opts = {
      height: '300',
      width: '600',
      playerVars: { // https://developers.google.com/youtube/player_parameters
       controls: false,
		rel:false,
		disablekb: 0
      },
	  allowfullscreen:false
    };
		
    return (
		<React.Fragment>
			<div className="container py-5 mt-0 mt-md-3">
				<YouTube
					videoId="GbvxJKzoIM4"       
					opts={opts}				  // defaults -> null
				/>
			</div>
			<div className="clear-fix"></div>
		</React.Fragment>
	
    )
  }
}	
export default Protect;	
export function AllUserOptions(){
	var HomeHeading = [];
	HomeHeading['top'] = 'Increase Your';
	HomeHeading['bottom'] = 'Business Visibility';
	HomeHeading['title1'] = 'Secure Your Data';
	HomeHeading['title2'] = 'Optimize Operations';
	HomeHeading['title3'] = 'Generate New Revenue';
	HomeHeading['description'] = 'Good marketing makes the company look smart. Great marketing makes the customer feel smart.';
	HomeHeading['get_started'] = 'Get Started';
	HomeHeading['contact_us'] = 'Contact Us';
	return HomeHeading;
}

export function CallToActions(){
	var CallToActions = [];
	
	CallToActions['title'] = 'Finibus Bonorum et Malorum';
	CallToActions['description'] = 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.';
	CallToActions['action1'] = 'WE WORK FOR YOU';
	CallToActions['action2'] = 'ON CALL 24/7';
	CallToActions['action3'] = 'LATEST TECH';
	CallToActions['get_started'] = 'Get Started';
	
	return CallToActions;
}



export function QuickResult(){
	var QuickResult = [];
	QuickResult['title'] = 'Get quick results with high throughput';
	QuickResult['description'] = 'The great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself.';
	QuickResult['read_more'] = 'Read More';
	
	return QuickResult;
}

export function Services(){
	var Services = [];
	
	Services['title'] = 'Our Services';
	Services['services_1_title'] = 'Responsive<b> Layout Template</b>';
	Services['services_1_description'] = 'Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.';
	Services['services_2_title'] = '<b>Landing Page</b> Analysis';
	Services['services_2_description'] = 'A perfect structure created after we analized trends in SaaS landing page designs. Analysis made to the most popular SaaS businesses.';
	Services['services_3_title'] = '<b>Smart</b> BEM <b>Grid</b>';
	Services['services_3_description'] = 'Blocks, Elements and Modifiers. A smart HTML/CSS structure that can easely be reused. Layout driven by the purpose of modularity.';
	Services['services_4_title'] = '<b>User</b> Friendly';
	Services['services_4_description'] = 'Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.';
	Services['services_5_title'] = '<b>Best Online</b> Security';
	Services['services_5_description'] = 'Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.';
	Services['services_6_title'] = '<b>Target</b> Audience';
	Services['services_6_description'] = 'Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.';
	
	
	return Services;
}

export function SuccessStories(){
	var SuccessStories = [];
	SuccessStories['title'] = 'Client Feedback';
	SuccessStories['description'] = 'Generally, every customer wants a product or service that solves their problem, worth their money, and is delivered with amazing customer service';
	SuccessStories['testimonials'] = [
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
										{
											description:'"No one can make you successful; the will to success comes from within. I have made this my motto.  I have internalized it to the point of understanding that the success of my actions and/or endeavors does not  depend on anyone else, and that includes a possible failure"',
											author:'Maria Allesi',
											company:'Italy Solutions',
											user_image:'avatar-5.jpg'
										},
									]
	
	return SuccessStories;
}

export function MoreFeatures(){
	var MoreFeatures = [];
	MoreFeatures['title'] = 'More Features';
	MoreFeatures['description'] = 'it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure.';
	
	return MoreFeatures;
}

export function Pricing(){
	var Pricing = [];
	Pricing['title'] = 'Flexible Pricing';
	Pricing['description'] = 'Generally, every customer wants a product or service that solves their problem, worth their money, and is delivered with amazing customer service';
	
	Pricing['monthly'] = [
							{
								title:'Free',
								price:'$0',
								button:'Sign Up',
								features:['<b>1</b> User Account','<b>10</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],popular:'none',
								buttonClass:'btn btn-outline-primary'
							},
							{
								title:'Pro',
								price:'$15',button:'Get Started',
								features:['<b>50</b> Users Account','<b>500</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								popular:'block',
								buttonClass:'btn btn-primary'
							},
							{
								title:'Ultra',
								price:'$29',
								button:'Contact Us',
								features:['<b>Unlimited</b> Users Account','<b>Unlimited</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								popular:'none',
								buttonClass:'btn btn-outline-primary'}
						];
	
	Pricing['yearly'] = [
							{
								title:'Free',
								price:'$0',
								button:'Sign Up',
								features:['<b>1</b> User Account','<b>10</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],buttonClass:'btn btn-outline-primary'},
							{
								title:'Pro',
								price:'$15',
								button:'Get Started',
								features:['<b>50</b> Users Account','<b>50</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								buttonClass:'btn btn-primary'},
							{
								title:'Ultra',
								price:'$29',
								button:'Contact Us',
								features:['<b>Unlimited</b> Users Account','<b>Unlimited</b> Team Members','<b>Unlimited</b> Emails Accounts','Set And Manage Permissions','API &amp; extension support','Developer support','A / B Testing'],
								buttonClass:'btn btn-outline-primary'}
						];
	
	return Pricing;
}

export function HomeSliderImages(){
	var HomeSliderImages = [];
	
	HomeSliderImages = [{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'},{image1:'amazon-logo.jpg'},{image1:'azure-logo.jpg'},{image1:'microsoft.jpg'},{image1:'checkpoint-logo.jpg'}];
	
	return HomeSliderImages;
}
import React, { Component } from 'react';

import Slider from 'react-slick/lib/slider';
import Dots from 'react-slick/lib/dots';


import "../../../scss/slick/slick.css";
import "../../../scss/slick/slick-theme.css";

import * as util from './HomeContent.js';
import Img from 'react-image';


class SuccessStories extends Component {

	
  render() {
  const settings = {
		  autoplay:false,
		  dots:true,
		  arrows: false,
		  infinite: true,
		  speed: 500,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		};
   		const SuccessStories = util.SuccessStories();								
    return (
		<div>
			
			<div className="container my-5 pt-0 pt-md-5">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">{SuccessStories['title']}</h2>
							<div className="clearfix"></div>
							<div className="brd mb-3"></div>				
							
						</div>
					</div>
				</div>	
			</div>

				<div className="container my-sm-3">
					<div className="row"> 
						<div className="testimonial-slider-wrap">
							<svg className="svg-testimonials-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,0 100,0 0,100"></polygon></svg>
							<svg className="svg-testimonials-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,0 100,0 100,100"></polygon></svg>
							<svg className="svg-testimonials-top" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,0 100,0 100,100"></polygon></svg>
							<svg className="svg-testimonials-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,100 100,0 100,100"></polygon></svg>		
						<div className="testimonial-slider">
						<Slider {...settings}>
						
						{SuccessStories['testimonials'].map((testimonial, index) =>
							<div className="testimonials__slide" key={index}>
								{/* <Img className="rounded-circle" src={require('../../../assets/images/'+testimonial.user_image)} alt="" /> */}
								<p>{testimonial.description}</p>
								<div className="testimonials__source">{testimonial.author} <a href="#/">{testimonial.company}</a></div>
							</div>
						)}
							
						</Slider>
							{Dots}	
						</div>			
					</div>	
				</div>
				</div>
		</div>	
	)
  }
}

export default SuccessStories;


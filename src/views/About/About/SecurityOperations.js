import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';
import * as util from './HomeContent.js';

 
class SecurityOperations extends Component {
	
	
  render() {
	 
		
    return (
		
		<div className="service-top-sec mt-5 mb-5">
			 <div className="container costum-container">
			  <div className="row align-items-center">
			  <div className="col-lg-12 cs-text pricing-table-sec">
				<div className="fadeDown" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000"> <h5>About  <b>Us</b> </h5></div>
				<div className="card-body" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000">
				<ul className="list-unstyled mt-3 mb-4"><b>FLINT.</b> Definition according to Webster:<br/>

					<li>a hard gray rock consisting of nearly pure chert, occurring chiefly as nodules in chalk</li>

					<li>a piece of flint, especially as flaked or ground in ancient times to form a tool or weapon</li>

					<li>a piece of flint used with steel to produce an igniting spark, e.g., in a flintlock gun, or (in modern use) a piece of an alloy used similarly, especially in a cigarette lighter</li>
				
				</ul>
					
					<p><b>This definition resonated with our founder, Ken Barrett.</b></p>

					<p>In the Bible, in the book of Isaiah, chapter 50, we hear from the Servant of God. He speaks of seeking to help others, of wanting to help them, even when it hurts, and of being fully equipped by God to do so. In verse 7 of this chapter, the Servant declares that he has set his “face like a flint”, and He knows that He will not be ashamed in His endeavors. He will succeed. This passage is talking about Jesus Christ, and as Ken Barrett read it, the passion behind it burned in his heart. He recognized his own God-given desire to seek out the underserved, to ensure they get what they need. His determination to do so caused him to set his face to the task like that hard, sharp flint stone, which not only points a direction with its edge, but ignites too.</p>

					<p>Ken’s extensive history in the corporate setting showed him that SMB’s that needed help, generally only received a bandaged solution. After going from bandage to bandage, he saw businesses wasting time and money. That’s how Flint Tech Solutions evolved into a business that ensured all clients were receiving customized care with customer service being priority #1!</p>

					<p>Flint Tech Solutions is now a company that has served clients in a wide variety of industries and has experienced team members in over 6 different countries. A company that offers ideas and technology transformations to organizations ensuring business goals are met and organizations are protected.</p>

					<p>In other words, the goal of Flint Tech Solutions was, is and always will be, to help you…</p>
				</div>
			  </div>
			  </div>
			 </div>
			</div>
	
    )
  }
}	
export default SecurityOperations;	
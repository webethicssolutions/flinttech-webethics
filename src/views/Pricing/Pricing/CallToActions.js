import React, { Component } from 'react';

import WorkIcon from '../../../assets/images/work-icon.png';
import TechIcon from '../../../assets/images/latest-tech-icon.png';
import OncallIcon from '../../../assets/images/oncall-icon.png';

class CallToActions extends Component {

	
  render() {

   										
    return (
		<div>
			<div className="container py-5 mt-0 mt-md-3">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Finibus Bonorum et Malorum</h2>
							<div className="clearfix"></div>
							<div className="brd mb-4"></div>
							<p className="sub-info fadeDown ts" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="300">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni 
							dolores eos qui ratione voluptatem sequi nesciunt.</p>
						</div>
					</div>
				</div>	
			</div>	

			<div className="container featured-icon-sec pb-0 pb-md-5">
					<div className="row">
					  <div className="col-md-4 text-center">
						<div className="hvr-float-shadow"><img className="img-fluid" src={WorkIcon} alt="" /></div>
						<h3 className="my-4 text-uppercase">We work for you</h3>
					  </div>
					  <div className="col-md-4 text-center">
						<div className="hvr-float-shadow"><img className="img-fluid" src={TechIcon} alt="" /></div>
						<h3 className="my-4 text-uppercase">On call 24/7</h3>
					  </div>
					  <div className="col-md-4 text-center">
						<div className="hvr-float-shadow"><img className="img-fluid" src={OncallIcon} alt="" /></div>
						<h3 className="my-4 text-uppercase">Latest Tech</h3>
					  </div>
					  <div className="col-12 mt-5 text-center">
						<a className="btn btn-primary fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300" href="#/"> GET STARTED</a>
					  </div>		  
					</div>
			</div>
		</div>	
	)
  }
}

export default CallToActions;
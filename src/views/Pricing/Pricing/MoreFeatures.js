import React, { Component } from 'react';

class MoreFeatures extends Component {
  render() {
    return (
		
		<div className="more-features">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">More Features</h2>
							<div className="clearfix"></div>
							<div className="brd"></div>				
							<p className="sub-info mt-4">it is pleasure, but because those who do not know how to pursue pleasure rationally 
							encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires 
							to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and 
							pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical 
							exercise, except to obtain some advantage from it? it is pleasure, but because those who do not know how to pursue pleasure 
							rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of 
							itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. 
							To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? 
							But who has any right to find fault with a man who chooses to enjoy a pleasure.</p>
						</div>
					</div>
			</div>	
			</div>				
			</div>	
	
    )
  }
}	
export default MoreFeatures;	
import React, { Component } from 'react';


import Image1 from '../../../assets/images/quick-result-img.png';


class QuickResult extends Component {
  render() {
    return (
		<div className="quick-result-sec mt-5">
			<div className="container">
				<div className="row align-items-center">
					<div className="col-lg-6">
						<div className="display-4 fadeDown ts" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="300">Get <b>quick results</b> with high throughput</div>
						<p className="my-4 fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">The great explorer of the truth, the master-builder of human happiness. No one rejects, 
						dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not 
						know how to pursue pleasure rationally encounter consequences that are extremely painful. 
						Nor again is there anyone who loves or pursues or desires to obtain pain of itself.</p>
						<a className="read-more-link" href="#/">Read More</a>
					</div>
					<div className="col-lg-6">
						<img className="img-fluid" src={Image1} alt="" />
					</div>
				</div>	
			</div>				
		</div>	
	
    )
  }
}	
export default QuickResult;	
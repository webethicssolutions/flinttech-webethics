import React, { Component } from 'react';

import BannerBottom from '../../../assets/images/sos-banner.jpg';

class HomeBottomBanner extends Component {

	
  render() {

   										
    return (
		<div className="banner-bottom"><img className="img-fluid" src={BannerBottom} alt=""/></div>	
	)
  }
}

export default HomeBottomBanner;
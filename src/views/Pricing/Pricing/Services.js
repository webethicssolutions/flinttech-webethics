import React, { Component } from 'react';

import layout from '../Services/layout.svg'
import analysis from '../Services/analysis.svg'
import grid from '../Services/grid.svg'
import SVG from 'react-inlinesvg';

class Services extends Component {

	
  render() {

   										
    return (
		<div>
			<div className="container my-5">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300" >Our Services</h2>
							<div className="clearfix"></div>
							<div className="brd"></div>
						</div>
					</div>
				</div>	
			</div>	
			
			
<div className="container service-sec">
        <div className="row">
          <div className="col-md-6 col-lg-4 text-center">
					<SVG src={layout} />
					<h3 className="my-4 text-uppercase">Responsive<b> Layout Template</b></h3>
					<p>Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.</p>
				  </div>
				  <div className="col-md-6 col-lg-4 text-center">
					
					<SVG src={analysis} />
					<h3 className="my-4 text-uppercase"><b>Landing Page</b> Analysis</h3>
					<p>A perfect structure created after we analized trends in SaaS landing page designs. Analysis made to the most popular SaaS businesses.</p>
				  </div>
				  <div className="col-md-6 col-lg-4 text-center">
					<SVG src={grid} />

					<h3 className="my-4 text-uppercase"><b>Smart</b> BEM <b>Grid</b></h3>
					<p>Blocks, Elements and Modifiers. A smart HTML/CSS structure that can easely be reused. Layout driven by the purpose of modularity.</p>
				  </div>
				  
				  <div className="col-md-6 col-lg-4 text-center">
					<SVG src={layout} />

					<h3 className="my-4 text-uppercase">Responsive<b> Layout Template</b></h3>
					<p>Responsive code that makes your landing page look good on all devices (desktops, tablets, and phones). Created with mobile specialists.</p>
				  </div>
				  <div className="col-md-6 col-lg-4 text-center">
					<SVG src={analysis} />

					<h3 className="my-4 text-uppercase"><b>Landing Page</b> Analysis</h3>
					<p>A perfect structure created after we analized trends in SaaS landing page designs. Analysis made to the most popular SaaS businesses.</p>
				  </div>
				  <div className="col-md-6 col-lg-4 text-center">
					
					<SVG src={grid} />
					<h3 className="my-4 text-uppercase"><b>Smart</b> BEM <b>Grid</b></h3>
					<p>Blocks, Elements and Modifiers. A smart HTML/CSS structure that can easely be reused. Layout driven by the purpose of modularity.</p>
				  </div>		  
					  
				</div>
		</div>

			
		</div>	
	)
  }
}

export default Services;
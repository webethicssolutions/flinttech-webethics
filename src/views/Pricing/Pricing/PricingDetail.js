import React, { Component } from 'react';


class PricingDetail extends Component {
	
	
  render() {
    return (
		
		<div>
			<div className="container my-5 pt-5">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeDown ts"  data-aos="fade-down" data-aos-duration="1000" data-aos-delay="300">Flexible Pricing</h2>
							<div className="clearfix"></div>
							<div className="brd"></div>				
							<p className="sub-info mt-4 fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Generally, every customer wants a product or service that solves their problem, worth their money, and is delivered with amazing customer service</p>
						</div>
					</div>
				</div>	
			</div>	



<div className="container pricing-table-sec my-5 pb-0 pb-sm-5">
<div className="row">
	<div className="col-12">
		<ul className="nav nav-pills justify-content-center mb-5" id="pills-tab" role="tablist">
		  <li className="nav-item">
			<a className="nav-link active" id="pills-monthly-tab" data-toggle="pill" href="#pills-monthly" role="tab" aria-controls="pills-home" aria-selected="true">Monthly</a>
		  </li>
		  <li className="nav-item">
			<a className="nav-link" id="pills-yearly-tab" data-toggle="pill" href="#pills-yearly" role="tab" aria-controls="pills-profile" aria-selected="false">Yearly</a>
		  </li>
		</ul>
	</div>
	
	<div className="col-12 mt-5">
		<div className="tab-content" id="pills-tabContent">
		  <div className="tab-pane fade show active" id="pills-monthly" role="tabpanel" aria-labelledby="pills-home-tab">
		  <div className="card-deck mb-3 text-center">
			<div className="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div className="card-header">
				<h4 className="my-0">Free</h4>
				<div className="clearfix"></div>
				<div className="brd"></div>					
			  </div>
			  <div className="card-body">
				<h1 className="card-title pricing-card-title pricing__value pricing__value--show">$0 <sub>/ mo</sub></h1>
				<ul className="list-unstyled mt-3 mb-4">
					<li><b>1</b> User Account</li>
					<li><b>10</b> Team Members</li>
					<li><b>Unlimited</b> Emails Accounts</li>	
					<li>Set And Manage Permissions</li>
					<li className="disabled">API &amp; extension support</li>	
					<li className="disabled">Developer support</li>
					<li className="disabled">A / B Testing</li>		
				</ul>
				<button type="button" className="btn btn-outline-primary fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Sign up</button>
			  </div>
			</div>
			<div className="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div className="card-header">
				<h4 className="my-0">Pro</h4>
				<div className="clearfix"></div>
				<div className="brd"></div>
				<div className="label">POPULAR</div>	
			  </div>
			  <div className="card-body">
				<h1 className="card-title pricing-card-title pricing__value pricing__value--show">$15 <sub>/ mo</sub></h1>
				<ul className="list-unstyled mt-3 mb-4">
					<li><b>50</b> User Account</li>
					<li><b>500</b> Team Members</li>
					<li><b>Unlimited</b> Emails Accounts</li>	
					<li>Set And Manage Permis sions</li>
					<li>API &amp; extension support</li>	
					<li>Developer support</li>
					<li className="disabled">A / B Testing</li>						
				</ul>
				<button type="button" className="btn btn-primary fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Get started</button>
			  </div>
			</div>
			<div className="card mb-4">
				<span></span>
				<span></span>
				<span></span>
				<span></span>			
			  <div className="card-header">
				<h4 className="my-0">Ultra</h4>
				<div className="clearfix"></div>
				<div className="brd"></div>					
			  </div>
			  <div className="card-body">
				<h1 className="card-title pricing-card-title pricing__value pricing__value--show">$29 <sub>/ mo</sub></h1>
				<ul className="list-unstyled mt-3 mb-4">
					<li><b>Unlimited</b> User Account</li>
					<li><b>Unlimited</b> Team Members</li>
					<li><b>Unlimited</b> Emails Accounts</li>	
					<li>Set And Manage Permissions</li>
					<li>API &amp; extension support</li>	
					<li>Developer support</li>
					<li>A / B Testing</li>
				</ul>
				<button type="button" className="btn btn-outline-primary fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Contact us</button>
			  </div>
			</div>
		  </div>
		  </div>
		  <div className="tab-pane fade" id="pills-yearly" role="tabpanel" aria-labelledby="pills-profile-tab">
		  <div className="card-deck mb-3 text-center">
			<div className="card mb-4">
			  <div className="card-header">
				<h4 className="my-0">Free</h4>
				<div className="clearfix"></div>
				<div className="brd"></div>					
			  </div>
			  <div className="card-body">
				<h1 className="card-title pricing-card-title pricing__value pricing__value--show">$0 <sub>/ mo</sub></h1>
				<ul className="list-unstyled mt-3 mb-4">
					<li><b>1</b> User Account</li>
					<li><b>10</b> Team Members</li>
					<li><b>Unlimited</b> Emails Accounts</li>	
					<li>Set And Manage Permissions</li>
					<li className="disabled">API &amp; extension support</li>	
					<li className="disabled">Developer support</li>
					<li className="disabled">A / B Testing</li>		
				</ul>
				<button type="button" className="btn btn-outline-primary fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Sign up</button>
			  </div>
			</div>
			<div className="card mb-4">
			  <div className="card-header">
				<h4 className="my-0">Pro</h4>
				<div className="clearfix"></div>
				<div className="brd"></div>			
			  </div>
			  <div className="card-body">
				<h1 className="card-title pricing-card-title pricing__value pricing__value--show">$15 <sub>/ mo</sub></h1>
				<ul className="list-unstyled mt-3 mb-4">
					<li><b>50</b> User Account</li>
					<li><b>500</b> Team Members</li>
					<li><b>Unlimited</b> Emails Accounts</li>	
					<li>Set And Manage Permis sions</li>
					<li>API &amp; extension support</li>	
					<li>Developer support</li>
					<li className="disabled">A / B Testing</li>						
				</ul>
				<button type="button" className="btn btn-primary fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Get started</button>
			  </div>
			</div>
			<div className="card mb-4">
			  <div className="card-header">
				<h4 className="my-0">Ultra</h4>
				<div className="clearfix"></div>
				<div className="brd"></div>					
			  </div>
			  <div className="card-body">
				<h1 className="card-title pricing-card-title pricing__value pricing__value--show">$29 <sub>/ mo</sub></h1>
				<ul className="list-unstyled mt-3 mb-4">
					<li><b>Unlimited</b> User Account</li>
					<li><b>Unlimited</b> Team Members</li>
					<li><b>Unlimited</b> Emails Accounts</li>	
					<li>Set And Manage Permissions</li>
					<li>API &amp; extension support</li>	
					<li>Developer support</li>
					<li>A / B Testing</li>
				</ul>
				<button type="button" className="btn btn-outline-primary fadeUp ts"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Contact us</button>
			  </div>
			</div>
		  </div>		  
		  </div>
		</div>	
	</div>	
</div>	
</div>
		</div>
	
    )
  }
}	
export default PricingDetail;	
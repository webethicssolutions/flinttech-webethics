import React, { Component } from 'react';
import Image1 from '../../../assets/images/amazon-logo.jpg';
import Image2 from '../../../assets/images/checkpoint-logo.jpg';
import Image3 from '../../../assets/images/azure-logo.jpg';
import Image4 from '../../../assets/images/microsoft.jpg';
import Image5 from '../../../assets/images/checkpoint-logo.jpg';

import Slider from 'react-slick/lib/slider';
import "../../../scss/slick/slick.css";
import "../../../scss/slick/slick-theme.css";


class HomeSlider extends Component {
	
  render() {
		document.title = "Home | Flinttech";	
	   const settings = {
		   autoplay:true,
		  dots: false,
		  infinite: true,
		  speed: 500,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		    responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}	
		]
		};
    return (
		<div className="container">
			<div className="row"> 
				<div className="brands-slider">
					<Slider {...settings}>
					  <img src={Image1} alt="" />
					  <img src={Image2} alt="" />
					  <img src={Image3} alt="" />
					  <img src={Image4} alt="" />
					  <img src={Image5} alt="" />
					</Slider>
				</div>		
			</div>	
		</div>
	
    )
  }
}	
export default HomeSlider;	
import React, { Component } from 'react';
import image1 from "../../../assets/images/avatar-5.jpg";
import Slider from 'react-slick/lib/slider';

import "../../../scss/slick/slick.css";
import "../../../scss/slick/slick-theme.css";


class SuccessStories extends Component {

	
  render() {
  const settings = {
		  autoplay:true,
		  dots:true,
		  arrows: false,
		  infinite: true,
		  speed: 500,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		};
   										
    return (
		<div>
			<div className="clearfix shadow-bar"></div>
			<div className="container my-5 pt-0 pt-md-5">
				<div className="row">
					<div className="col-12">
						<div className="heading-bar text-center">
							<h2 className="fadeUp ts" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="200">Success Stories</h2>
							<div className="clearfix"></div>
							<div className="brd mb-3"></div>				
							<p className="sub-info fadeUp ts" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">Generally, every customer wants a product or service that solves their problem, worth their money, and is delivered with amazing customer service</p>
						</div>
					</div>
				</div>	
			</div>

				<div className="container">
					<div className="row"> 
						<div className="testimonial-slider-wrap">
							<svg className="svg-testimonials-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,0 100,0 0,100"></polygon></svg>
							<svg className="svg-testimonials-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,0 100,0 100,100"></polygon></svg>
							<svg className="svg-testimonials-top" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,0 100,0 100,100"></polygon></svg>
							<svg className="svg-testimonials-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="#fff" points="0,100 100,0 100,100"></polygon></svg>		
						<div className="testimonial-slider">
						<Slider {...settings}>
						  <div className="testimonials__slide">
							<img className="rounded-circle" src={image1} alt="" />
						  <p>"No one can make you successful; the will to success comes from within.' I've made this my motto. 
						  I've internalized it to the point of understanding that the success of my actions and/or endeavors doesn't 
						  depend on anyone else, and that includes a possible failure"</p>
						  <div className="testimonials__source">Maria Allesi <a href="#/">Italy Solutions</a></div>
						  </div>
						  <div className="testimonials__slide">
						  <img className="rounded-circle" src={image1} alt="" />
						  <p>"No one can make you successful; the will to success comes from within.' I've made this my motto. 
						  I've internalized it to the point of understanding that the success of my actions and/or endeavors doesn't 
						  depend on anyone else, and that includes a possible failure"</p>
						  <div className="testimonials__source">Maria Allesi <a href="#/">Italy Solutions</a></div>
						  </div>
						  <div className="testimonials__slide">
						  <img className="rounded-circle" src={image1} alt="" />
						  <p>"No one can make you successful; the will to success comes from within.' I've made this my motto. 
						  I've internalized it to the point of understanding that the success of my actions and/or endeavors doesn't 
						  depend on anyone else, and that includes a possible failure"</p>
						  <div className="testimonials__source">Maria Allesi <a href="#/">Italy Solutions</a></div>
						  </div>
						  <div className="testimonials__slide">
						  <img className="rounded-circle" src={image1} alt="" />
						  <p>"No one can make you successful; the will to success comes from within.' I've made this my motto. 
						  I've internalized it to the point of understanding that the success of my actions and/or endeavors doesn't 
						  depend on anyone else, and that includes a possible failure"</p>
						  <div className="testimonials__source">Maria Allesi <a href="#/">Italy Solutions</a></div>
						  </div>
						</Slider>
								
						</div>			
					</div>	
				</div>
				</div>
		</div>	
	)
  }
}

export default SuccessStories;


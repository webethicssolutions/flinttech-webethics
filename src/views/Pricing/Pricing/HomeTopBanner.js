import React, { Component } from 'react';
//import Tu from 't-scroll/public/theme/js/t-scroll.js';

class HomeTopBanner extends Component {
	
	
  render() {
	  
							
    return (
		
		<div className="main-banner" >
			<div className="container">
				<div className="row">
					<div className="col col-xl-6 text-center text-xl-left">
					<h1 className="display-3" data-aos="fade-down" data-aos-duration="1200" data-aos-delay="500" > 
						<span className="font-weight-light aos-animate aos-init pr-3" >Increase Your</span>
						<br />
						<span className="font-weight-bold aos-animate aos-init pr-3" >Business Visibility</span>
					</h1>
					{/* <h1 className="display-3">
						<span className="font-weight-light lightSpeedLeft aos-animate aos-init" data-aos="zoom-in" >I</span>
						<span className="font-weight-light lightSpeedLeft aos-animate aos-init" data-aos="zoom-in" data-aos-easing="ease" date-aos-delay="1000">n</span>
						<span className="font-weight-light lightSpeedLeft aos-animate aos-init" data-aos="zoom-in" data-aos-easing="ease" date-aos-delay="2000">c</span>
						<span className="font-weight-light lightSpeedLeft aos-animate aos-init" data-aos="zoom-in" data-aos-easing="ease" date-aos-delay="4000">r</span>
						<span className="font-weight-light lightSpeedLeft aos-animate aos-init" data-aos="zoom-in" data-aos-easing="ease" date-aos-delay="6000">e</span>
						<span className="font-weight-light lightSpeedLeft aos-animate aos-init" data-aos="zoom-in" data-aos-easing="ease" date-aos-delay="8000">a</span>
						<span className="font-weight-light lightSpeedLeft aos-animate aos-init" data-aos="zoom-in" data-aos-easing="ease" date-aos-delay="8000">s</span>
						<span className="font-weight-light lightSpeedLeft aos-animate pr-3" data-aos="zoom-in" data-aos-easing="ease" date-aos-delay="8000">e</span>
						<span className="font-weight-light lightSpeedLeft aos-animate" data-aos="zoom-in" date-aos-delay="50">Y</span>
						<span className="font-weight-light lightSpeedLeft aos-animate"  data-aos="zoom-in" date-aos-delay="50">o</span>
						<span className="font-weight-light lightSpeedLeft aos-animate" data-aos="zoom-in" date-aos-delay="50">u</span>
						<span className="font-weight-light lightSpeedLeft aos-animate" data-aos="zoom-in" date-aos-delay="50">r</span>
						<br />
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">B</span>
						<span className="lightSpeedLeft aos-animate"  data-aos="zoom-in" date-aos-delay="50">u</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">s</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">i</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">n</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">e</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">s</span>
						<span className="lightSpeedLeft  aos-animate pr-3" data-aos="zoom-in" date-aos-delay="50">s</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">V</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">i</span>
						<span className="lightSpeedLeft  aos-animate" data-aos="zoom-in" date-aos-delay="50">s</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">i</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">b</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">i</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">l</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">i</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">t</span>
						<span className="lightSpeedLeft  aos-animate"  data-aos="zoom-in" date-aos-delay="50">y</span>		
							
					</h1> */}
					
					<p className="lead fadeUp  aos-init aos-animate">Secure Your Data | Optimize Operations | Generate New Revenue </p>
					<p className="fadeUp  aos-init aos-animate">Good marketing makes the company look smart. Great marketing makes the customer feel smart.</p>
					<div className="button-wrap mt-4 mt-sm-5" data-aos="fade-up"  data-aos-duration="1200" data-aos-delay="500">
						<a className="btn btn-primary fadeUp ts " href="#/"> GET STARTED</a>
						<a className="btn btn-outline-primary fadeUp ts" href="#/">Contact Us</a></div>				
					</div>
					
				</div>
			</div>	
		</div>
	
    )
  }
}	
export default HomeTopBanner;	
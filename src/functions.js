export function validateTask(name,email,phone) {
	const errors = [];  
	if (name.length <= 0) {
		errors.push("Please enter your Name.");
	} 
	if (email.length <= 0) {
		errors.push("Please enter your Email.");
	} 
	if( !validateEmail(email)){
		errors.push("Please enter valid Email.");
	} 
	if (phone.length <= 0) {
		errors.push("Please enter your Phone Number.");
	} 
	
   return errors;
}

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}
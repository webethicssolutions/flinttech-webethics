export default {
  items: [
    {
      name: 'About',
      url: '/about',
      icon: 'icon-speedometer',
    },
  ],
};
